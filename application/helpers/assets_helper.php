<?php  if ( ! defined('BASEPATH')) exit('No direct access allowed');

/**
 * CodeIgniter CSS JS Helpers
 *
 */

//Function to add Javascript files to header page
if(!function_exists('add_js')){
	function add_js($file='')
	{
		$str = '';
		$ci = &get_instance();
		$js_array  = $ci->config->item('js_array');

		if(empty($file)){
			return;
		}

		if(is_array($file)){
			if(!is_array($file) && count($file) <= 0){
				return;
			}
			foreach($file AS $item){
				$js_array[] = $item;
			}
			$ci->config->set_item('js_array',$js_array);
		}else{
			$str = $file;
			$js_array[] = $str;
			$ci->config->set_item('js_array',$js_array);
		}
	}
}

//Function to add CSS files to header page
if(!function_exists('add_css')){
	function add_css($file='')
	{
		$str = '';
		$ci = &get_instance();
		$css_array = $ci->config->item('css_array');

		if(empty($file)){
			return;
		}

		if(is_array($file)){
			if(!is_array($file) && count($file) <= 0){
				return;
			}
			foreach($file AS $item){
				$css_array[] = $item;
			}
			$ci->config->set_item('css_array',$css_array);
		}else{
			$str = $file;
			$css_array[] = $str;
			$ci->config->set_item('css_array',$css_array);
		}
	}
}
//Function to put headers in layout
if(!function_exists('put_headers')){
	function put_headers()
	{
		$str = '';
		$ci = &get_instance();
		$css_array = $ci->config->item('css_array');
		if (!empty($css_array)) {
			foreach($css_array AS $item){
				if(is_remote($item)){
					$str .= '<link rel="stylesheet" href="'.$item.'" type="text/css" />'."\n";
				}else{
					$str .= '<link rel="stylesheet" href="'.base_url().'assets/'.$item.'" type="text/css" />'."\n";
				}
			}
		}
		return $str;
	}
}
//Function to put footer in layout
if(!function_exists('put_footer')){
	function put_footer()
	{
		$str = '';
		$ci = &get_instance();
		$js_array  = $ci->config->item('js_array');
		if(!empty($js_array)){
			foreach($js_array AS $item){
				if(is_remote($item)){
					$str .= '<script type="text/javascript" src="'.$item.'"></script>'."\n";
				}else{
					$str .= '<script type="text/javascript" src="'.base_url().'assets/'.$item.'"></script>'."\n";
				}
			}
		}
		return $str;
	}
}
//Function to use add css and js while using put headers and put footer
if(!function_exists('is_remote')){
	function is_remote($url)
	{
		if(strpos($url, '//cdnjs') !== false || strpos($url, '//cdn') !== false){
			return true;
		}else{
			return false;
		}
	}
}