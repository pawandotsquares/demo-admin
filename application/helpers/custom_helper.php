<?php  if ( ! defined('BASEPATH')) exit('No direct access allowed');

/**
 * CodeIgniter Custom Helpers
 *
 */

if ( ! function_exists('generateHash'))
{
	function generateHash($plainText, $salt = null)
	{
		if ($salt === null)
		{
			$salt = substr(md5(uniqid(rand(), true)), 0, 25);
		}
		else
		{
			$salt = substr($salt, 0, 25);
		}

		return $salt . sha1($salt . $plainText);
	}
}

if ( ! function_exists('pr'))
{
	function pr($array)
	{
		echo "<pre>";
		print_r($array);
		echo "</pre>";
	}
}
if ( ! function_exists('prd'))
{
	function prd($array)
	{
		echo "<pre>";
		print_r($array);
		echo "</pre>";
		die;
	}
}

//---------------------------------------------------------------------------------

if ( ! function_exists('clean')) {
	function clean($str) {
		$str = @trim($str);
		if(get_magic_quotes_gpc()) {
			$str = stripslashes($str);
		}
		return $str;
	}
}

if ( ! function_exists('getSidebar')){
	function getSidebar($role_id){
		$CI =& get_instance();
		$CI->load->model('privileges');
		$sidebar = $CI->privileges->get_controller_and_action_by_role_id($role_id );
		return $sidebar;
	}
}

/* End of file array_helper.php */
/* Location: ./system/helpers/custom_helper.php */