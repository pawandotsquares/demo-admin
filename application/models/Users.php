<?php

class Users extends MY_Model {

	var $table_users = "users";
	var $table_business = "business";
	var $table_customer = "customer";
	var $table_assign_operative = "assign_operative";

	var $datestring = "%Y-%m-%d";
	var $dateStringWithTime = "%Y-%m-%d %H:%i:%s";
	var $currentDate = '';
	var $currentDateTime = '';

	function __construct() {
		parent::__construct();
		$this->load->database();

		$this->currentDate = mdate($this->datestring, time());
		$this->currentDateTime = mdate($this->dateStringWithTime, time());
		$this->currentTime = time();
	}

    /**
     * This is a generic function get business data
     * @return json encode array that contain data,total records
     */
    function getBusinessData($where = NULL,$select =  '',$order_by= NULL, $offset=0, $limit=10,$where_in = false,$where_not_in = false){

    	$this->db->select($select, FALSE);
    	$this->db->from($this->table_users.' as U');
    	$this->db->Join($this->table_business.' as B','B.user_id = U.user_id','LEFT');

    	if(!empty($where)){
    		$this->db->where($where);
    	}
    	if($where_in){
    		$this->db->where_in($where_in[0], $where_in[1]);
    	}
    	if($where_not_in){
    		$this->db->where_not_in($where_not_in[0], $where_not_in[1]);
    	}
    	if(!empty($order_by)){
    		$this->db->order_by($order_by[0],$order_by[1]);
    	}
    	$result=  array();
        //getting number of records to show pagination in datatabel
    	if($limit){
    		$tempdb          = clone $this->db;
    		$totaldata       = $tempdb->get();
    		$result['total'] = $totaldata->result_id->num_rows;
    		$this->db->limit($limit, $offset);
    	}
    	$result['data'] =  $this->db->get()->result_array();
    	return $result;
    }

    /**
     * This is a generic function get business data
     * @return json encode array that contain data,total records
     */
    function getParkingOperativeData($where = NULL,$select =  '',$order_by= NULL, $offset=0, $limit=10,$where_in = false,$where_not_in = false){

        $this->db->select($select, FALSE);
        $this->db->from($this->table_users.' as U');
        $this->db->Join($this->table_assign_operative.' as O','O.parking_operative_id = U.user_id','LEFT');

        if(!empty($where)){
            $this->db->where($where);
        }
        if($where_in){
            $this->db->where_in($where_in[0], $where_in[1]);
        }
        if($where_not_in){
            $this->db->where_not_in($where_not_in[0], $where_not_in[1]);
        }
        if(!empty($order_by)){
            $this->db->order_by($order_by[0],$order_by[1]);
        }
        $result=  array();
        //getting number of records to show pagination in datatabel

        $result =  $this->db->get()->result_array();
        #prd($result);
        return $result;
    }

}

