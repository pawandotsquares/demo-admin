<?php
class Controllers extends MY_Model {

	var $table = "controllers";
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	/**
	 * This is a generic function to add controller dynamic
	 * @return redirect page
	 */
	public function add_controller($controller_name,$alias,$ordering = false)
	{
		$this->db->select('controller_id');
		$this->db->where('controller_name',$controller_name);
		$this->db->from($this->table);
		$query = $this->db->get();
		$result =  $query->result_array();
		if(count($result))
		{
			$this->session->set_flashdata('message','<font style="color:red;">This controller name already exists.</font>');
			redirect(base_url().'admin/privilege/add_controller');
		}
		else
		{
			$data['controller_name']= $controller_name;
			$data['controller_url']= '/'.$controller_name;
			$data['controller_alias']= ucfirst($alias);
			$data['ordering']= $ordering;
			if ( $this->db->insert($this->table,$data) )
			{
				$this->session->set_flashdata('message','Controller added successfully.</font>');
				redirect(base_url().'admin/privilege/index');
			}
		}
		$this->db->close();
	}
}