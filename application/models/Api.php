<?php
class Api extends MY_Model {

	var $table_users 		    = 'users';
	var $table_business 		= 'business';
	var $table_customer 		= 'customer';

	var $datestring = "%Y-%m-%d";
	var $dateStringWithTime = "%Y-%m-%d %H:%i:%s";
	var $currentDate = '';
	var $currentDateTime = '';

	function __construct() {
		parent::__construct();
		$this->load->database();

		$this->currentDate = mdate($this->datestring, time());
		$this->currentDateTime = mdate($this->dateStringWithTime, time());
		$this->currentTime = time();
	}

	/**
	* get_business_data.
	* @param where condition,select and order_by
	* @return Business list
	*/
	function get_business_data($where = NULL,$select =  '*',$order_by= NULL){

		$this->db->select($select, FALSE);
		$this->db->from($this->table_users.' AS U');
		$this->db->join($this->table_business.' AS B','B.user_id = U.user_id','INNER');
		$this->db->join($this->table_assign_operative.' AS O','B.user_id = O.business_id','INNER');

		if(!empty($where)){
			$this->db->where($where, FALSE);
		}
		if(!empty($order_by)){
			$this->db->order_by($order_by[0],$order_by[1]);
		}else{
			$this->db->order_by('U.user_id','DESC');
		}
		$result=array();
		$result =  $this->db->get()->result_array();
		return $result;
	}

	/**
	* get_business_data.
	* @param where condition,select and order_by
	* @return Business list
	*/
	function get_business_user_data($where = NULL,$select =  '*',$order_by= NULL){

		$this->db->select($select, FALSE);
		$this->db->from($this->table_users.' AS U');
		$this->db->join($this->table_business.' AS B','B.user_id = U.user_id','LEFT');

		if(!empty($where)){
			$this->db->where($where, FALSE);
		}

		if(!empty($order_by)){
			$this->db->order_by($order_by[0],$order_by[1]);
		}else{
			$this->db->order_by('U.user_id','DESC');
		}
		$result=array();
		$result =  $this->db->get()->result_array();
		return $result;
	}
}
?>