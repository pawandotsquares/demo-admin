<?php
class Roles extends MY_Model {

	var $table = "roles";
	var $table_privileges = "privileges";
    var $datestring = "%Y-%m-%d";
	var $dateStringWithTime = "%Y-%m-%d %H:%i:%s";
	var $currentDate = '';
    var $currentDateTime = '';

	function __construct(){
        parent::__construct();
        $this->load->database();
    }

	public function show_all_roles(){
		$result = $this->db
				->select('*')
				->from($this->table)
				->get();
		return $result->result_array();
	}

	public function show_all_active_roles(){
		$this->db->select();
		$this->db->from($this->table);
		$this->db->where('role_is_active',1);
		$this->db->where('role_is_deleted',0);
		$result = $this->db->get();
		return $result->result_array();
        $this->db->close();
	}


	public function update_role($role_id,$data){
		$this->db->where('role_id', $role_id);
		$this->db->update($this->table, $data);
        $this->db->close();
	}

	public function delete_role($role_id,$data){
		$this->db->where('role_id', $role_id);
		$this->db->update($this->table,$data);
        $this->db->close();
	}


}