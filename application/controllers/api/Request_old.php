<?php defined('BASEPATH') OR exit('No direct access allowed');

/**
* Example
*
* This is an example of a few basic user interaction methods you could use
* all done with a hardcoded array.
*
* @package		CodeIgniter
* @subpackage	Rest Server
* @category	    Controller
* @author		Phil Sturgeon
* @link		http://philsturgeon.co.uk/code/
*/


require APPPATH.'/libraries/REST_Controller.php';

class Request extends REST_Controller
{

	function __construct(){
		parent::__construct();
		$this->load->model('api');

        $security=$this->api->getRecords('security');

        $headers = getallheaders();
        $headers = array_change_key_case($headers,CASE_LOWER);
        if(!array_key_exists('apikey', $headers)){
            $returnArray = array(
                'status' => false,
                'message' => 'Please provide a apikey.',
                'statusCode' => '500'
            );
            echo json_encode($returnArray, 200);
            die;
        }
        if(md5($security[0]['apikey'])!=$headers['apikey']){
            $returnArray = array(
                'status' => false,
                'message' => 'Please provide valid apikey.',
                'statusCode' => '500'
            );
            echo json_encode($returnArray, 200);
            die;
        }
	}

    /**
     * login business user.
     * @param  email,password
     * @return json encode array that contain flag, message
     */
    function business_login_post() {

    	set_time_limit(0);

        //checking email required field
    	if (!trim($this->post('email'))) {
    		$returnArray = array(
    			'status' => false,
    			'message' => 'email field is required.',
    			'statusCode' => '500'
    		);
    		echo json_encode($returnArray, 200);
    		die;
    	}
    	//checking valid email formate
    	if (!filter_var($this->post('email'), FILTER_VALIDATE_EMAIL)) {
    		$returnArray = array(
    			'status' => false,
    			'message' => 'Invalid email format',
    			'statusCode' => '500'
    		);
    		echo json_encode($returnArray, 200);
    		die;
    	}
        //checking password required field
    	if (!trim($this->post('password'))) {
    		$returnArray = array(
    			'status' => false,
    			'message' => 'password field is required.',
    			'statusCode' => '500'
    		);
    		echo json_encode($returnArray, 200);
    		die;
    	}

    	$email    = $this->post('email');
    	$password = $this->post('password');

    	$where_salt = array(
    		'user_email'=>$email
    	);
    	//fetching salt to encrypting password
    	$salt = $this->api->getSalt($this->api->table_users, $where_salt);
    	//encrypting password to get records
    	$password = generateHash($password,$salt);

    	//fatching records from db using provided email and password
    	$where_user = array(
    		'user_email'=>$email,
    		'user_password'=>$password,
    		'role_id'=>2
    	);
    	$userdetails = $this->api->getRecords($this->api->table_users,$where_user);

    	if (!empty($userdetails)) {

    		//business account activated or not
    		if($userdetails[0]['user_is_active'] == '0'){
    			$returnArray = array(
    				'status' => false,
    				'statusCode' => '500',
    				'message' => 'Your account has been deactivated by admin.'
    			);
    			echo json_encode($returnArray, 200);
    			die;
    		}
    		$returnArray = array(
    			'status' => true,
    			'statusCode' => '200',
    			'message' => 'login successfully',
    			'data' => $userdetails
    		);
    		echo json_encode($returnArray, 200);
    		die;
    	}else{

    		$returnArray = array(
    			'status' => false,
    			'statusCode' => '500',
    			'message' => 'Account is deleted or incorrect login credentials'
    		);
    		echo json_encode($returnArray, 200);
    		die;

    	}
    }

    /**
     * register User.
     * @return json encode array that contain flag,message
     * @param  name,password,email,location
     */
    function business_register_post() {
    	$register_list = $this->post();

        //$Required_arr: added all required fields
    	$required_arr = array('name','email','location','password');

    	//Checking required fields key exists or not
    	foreach ($required_arr as $key => $val) {
    		if (!array_key_exists($val, $register_list)) {
    			$returnArray = array(
    				'status' => false,
    				'message' => str_replace("_", " ", $val) . ' field is required.',
    				'statusCode' => '500'
    			);
    			echo json_encode($returnArray, 200);
    			die;
    		}
    	}
    	//Checking required fields value exists or not
    	foreach ($required_arr as $key => $val) {
    		if (!$register_list[$val]) {
    			$returnArray = array(
    				'status' => false,
    				'message' => str_replace("_", " ", $val) . ' field is required.',
    				'statusCode' => '500'
    			);
    			echo json_encode($returnArray, 200);
    			die;
    		}
    	}
    	//checking if records already exists with provided email id
    	$condition_array = array('user_email' => $this->post('email'));
    	$email_exist = $this->api->checkRecord($this->api->table_users, $condition_array);

    	if ($email_exist) {
    		$error = 1;
    		$message = "This email already exists in the system, please try with another email.";
    		$returnArray = array(
    			'status' => false,
    			'message' => $message,
    			'statusCode' => '500'
    		);
    		echo json_encode($returnArray, 200);
    		die;
    	}else{
    		$save_user_data = array(
    			'user_email' => $this->post('email'),
    			'user_name' => $this->post('name'),
    			'role_id'=>2,
    			'user_password' => generateHash($this->post('user_password'))
    		);
            //save function is used to save or update records in a given table
    		$last_id = $this->api->save($this->api->table_users, $save_user_data);
            // user data end here
    		if ($last_id) {

    			$save_business_data = array(
    				'user_id' => $last_id,
    				'location' => $this->post('location'),
    			);
    			$last_id = $this->api->save($this->api->table_business, $save_business_data);
    			$returnArray = array(
    				'status' => true,
    				'message' => 'Business registered successfully',
    				'statusCode' => '200',
    			);
    			echo json_encode($returnArray, 200);
    			die;
    		}
    		else {
    			$returnArray = array(
    				'status' => false,
    				'message' => 'Error occurred.',
    				'statusCode' => '500'
    			);
    			echo json_encode($returnArray, 200);
    			die;
    		}
    	}
    }

    /**
     * login parking operative.
     * @param  email,password
     * @return json encode array that contain flag, message
     */
    function parking_operative_login_post() {

    	set_time_limit(0);

        //checking email required field
    	if (!trim($this->post('email'))) {
    		$return_array = array(
    			'status' => false,
    			'message' => 'email field is required.',
    			'statusCode' => '500'
    		);
    		echo json_encode($return_array, 200);
    		die;
    	}
    	//checking valid email formate
    	if (!filter_var($this->post('email'), FILTER_VALIDATE_EMAIL)) {
    		$return_array = array(
    			'status' => false,
    			'message' => 'Invalid email format',
    			'statusCode' => '500'
    		);
    		echo json_encode($return_array, 200);
    		die;
    	}
        //checking password required field
    	if (!trim($this->post('password'))) {
    		$return_array = array(
    			'status' => false,
    			'message' => 'password field is required.',
    			'statusCode' => '500'
    		);
    		echo json_encode($return_array, 200);
    		die;
    	}

    	$email    = $this->post('email');
    	$password = $this->post('password');

    	$where_salt = array(
    		'user_email'=>$email
    	);
    	//fetching salt to encrypting password
    	$salt = $this->api->getSalt($this->api->table_users, $where_salt);
    	//encrypting password to get records
    	$password = generateHash($password,$salt);

    	//fatching records from db using provided email and password
    	$where_user = array(
    		'user_email'=>$email,
    		'user_password'=>$password,
    		'role_id'=>3
    	);
    	$select = 'user_id,user_name,user_email,contact_number,created_time,user_is_active';
    	$user_details = $this->api->getRecords($this->api->table_users,$where_user,$select);

    	if (!empty($user_details)) {

    		//checking parking officer's account activated or not
    		if($user_details[0]['user_is_active'] == '0'){
    			$return_array = array(
    				'status' => false,
    				'statusCode' => '500',
    				'message' => 'Your account has been deactivated by admin.'
    			);
    			echo json_encode($return_array, 200);
    			die;
    		}
    		$return_array = array(
    			'status' => true,
    			'statusCode' => '200',
    			'message' => 'login successfully',
    			'data' => $user_details
    		);
    		echo json_encode($return_array, 200);
    		die;
    	}else{
    		$return_array = array(
    			'status' => false,
    			'statusCode' => '500',
    			'message' => 'Account is deleted, or incorrect login credentials.'
    		);
    		echo json_encode($return_array, 200);
    		die;
    	}
    }

    /**
    * Business List.
    * @param parking_operative_id
    * @return json encode array that contain flag,message
    */
    function business_list_post(){

    	$parking_operative_data = $this->post();

    	//$Required_arr: added all required fields
    	$required_arr = array('parking_operative_id');

    	//Checking required fields key exists or not
    	foreach($required_arr as $key=>$val){
    		if(!array_key_exists($val, $parking_operative_data )){
    			$return_array = array(
    				'status'    => false,
    				'message' => $val.' field is required.',
    				'statusCode'=>'500'
    			);
    			echo json_encode($return_array,200); die;
    		}
    	}

    	//Checking required fields value exists or not
    	foreach($required_arr as $key=>$val){
    		if(!$parking_operative_data[$val]){
    			$return_array = array(
    				'status'    => false,
    				'message' => $val.' field is required.',
    				'statusCode'=>'500'
    			);
    			echo json_encode($return_array,200); die;
    		}
    	}

    	$parking_operative_id = $parking_operative_data['parking_operative_id'];

    	//fatching business lists from db using provided parking_operative_id
    	$where_user = array(
    		'O.officer_id'=>$parking_operative_id,
    		'U.user_is_active'=>'1',
    		'U.user_is_deleted'=>'0',
    	);
    	$select = 'U.user_id as business_id,U.user_name,U.user_email,U.contact_number,B.business_type,B.business_image,B.location';
    	$business_data = $this->api->get_business_data($where_user,$select);

    	if(!empty($business_data)){
    		$return_array = array(
    			'status' => true,
    			'statusCode' => '200',
    			'message' => 'Business List',
    			'business_list' => $business_data
    		);
    		echo json_encode($return_array, 200);
    		die;

    	}else{
    		$return_array = array(
    			'status'=>false,
    			'message'=>'No record found.',
    			'statusCode'=>'500'
    		);
    		echo json_encode($return_array,200); die;
    	}

    }
    /**
    * Customer parking entries.
    * @param business_id
    * @return json encode array that contain flag,message
    */
    function create_customer_parking_entries_post(){

    	$parking_operative_data = $this->post();

        //$Required_arr: added all required fields
    	$required_arr = array('business_id','parking_officer_id','customer_name','contact_number','vehicle_registration_number','in_time');

        //Checking required fields key exists or not
    	foreach($required_arr as $key=>$val){
    		if(!array_key_exists($val, $parking_operative_data )){
    			$return_array = array(
    				'status'    => false,
    				'message' => $val.' field is required.',
    				'statusCode'=>'500'
    			);
    			echo json_encode($return_array,200); die;
    		}
    	}

        //Checking required fields value exists or not
    	foreach($required_arr as $key=>$val){
    		if(!$parking_operative_data[$val]){
    			$return_array = array(
    				'status'    => false,
    				'message' => $val.' field is required.',
    				'statusCode'=>'500'
    			);
    			echo json_encode($return_array,200); die;
    		}
    	}

    	$business_id         = $parking_operative_data['business_id'];
    	$parking_officer_id  = $parking_operative_data['parking_officer_id'];
    	$customer_name       = $parking_operative_data['customer_name'];
    	$contact_number      = $parking_operative_data['contact_number'];
    	$vehicle_registration_number = $parking_operative_data['vehicle_registration_number'];
    	$in_time             = $parking_operative_data['in_time'];
        $in_time             = date('Y-m-d H:i:s', $in_time);
    	$customer_user_data = array(
    		'business_id'           => $business_id,
    		'parking_officer_id'    => $parking_officer_id,
    		'customer_name'         => $customer_name,
    		'contact_number'        => $contact_number,
    		'vehicle_registration_number' => $vehicle_registration_number,
    		'in_time'               => $in_time,
    		'created_time'          => $this->api->currentDateTime,

    	);
        //save function is used to save or update records in a given table
    	$last_id = $this->api->save($this->api->table_customer, $customer_user_data);
        // customer data end here
    	if ($last_id) {
    		$returnArray = array(
    			'status' => true,
    			'message' => 'Customer data added successfully',
    			'statusCode' => '200',
    		);
    		echo json_encode($returnArray, 200);
    		die;
    	}
    	else {
    		$returnArray = array(
    			'status' => false,
    			'message' => 'Error occurred.',
    			'statusCode' => '500'
    		);
    		echo json_encode($returnArray, 200);
    		die;
    	}

    }
    /**
    * Customer parking entries.
    * @param business_id
    * @return json encode array that contain flag,message
    */
    function view_customer_parking_entries_post(){

    	$parking_operative_data = $this->post();

    	//$Required_arr: added all required fields
    	$required_arr = array('business_id');

    	//Checking required fields key exists or not
    	foreach($required_arr as $key=>$val){
    		if(!array_key_exists($val, $parking_operative_data )){
    			$return_array = array(
    				'status'    => false,
    				'message' => $val.' field is required.',
    				'statusCode'=>'500'
    			);
    			echo json_encode($return_array,200); die;
    		}
    	}

    	//Checking required fields value exists or not
    	foreach($required_arr as $key=>$val){
    		if(!$parking_operative_data[$val]){
    			$return_array = array(
    				'status'    => false,
    				'message' => $val.' field is required.',
    				'statusCode'=>'500'
    			);
    			echo json_encode($return_array,200); die;
    		}
    	}

    	$business_id = $parking_operative_data['business_id'];

    	//fatching customer records from db using provided business_id
    	$where_customer = array(
    		'business_id'=>$business_id,
    	);
    	if(isset($parking_operative_data['parking_officer_id'])){
    		$where_customer['parking_officer_id'] = $parking_operative_data['parking_officer_id'];
    	}
    	if(isset($parking_operative_data['created_time'])){
    		$where_customer['created_time'] = $parking_operative_data['created_time'];
    	}
    	$select = 'customer_name,contact_number,vehicle_registration_number,in_time,out_time,parking_status';
    	$customer_data = $this->api->getRecords($this->api->table_customer,$where_customer,$select);

    	//output
    	if(!empty($customer_data)){
    		$return_array = array(
    			'status' => true,
    			'statusCode' => '200',
    			'message' => 'Customer List',
    			'customer_list' => $customer_data
    		);
    		echo json_encode($return_array, 200);
    		die;

    	}else{
    		$return_array = array(
    			'status'=>false,
    			'message'=>'No record found.',
    			'statusCode'=>'500'
    		);
    		echo json_encode($return_array,200); die;
    	}

    }

    /**
    * customer_search.
    * @param search_keyword
    * @return json encode array that contain flag,message
    */
    function customer_search_post(){

    	$parking_operative_data = $this->post();

    	//$Required_arr: added all required fields
    	$required_arr = array('business_id');

    	//Checking required fields key exists or not
    	foreach($required_arr as $key=>$val){
    		if(!array_key_exists($val, $parking_operative_data )){
    			$return_array = array(
    				'status'    => false,
    				'message' => $val.' field is required.',
    				'statusCode'=>'500'
    			);
    			echo json_encode($return_array,200); die;
    		}
    	}

    	//Checking required fields value exists or not
    	foreach($required_arr as $key=>$val){
    		if(!$parking_operative_data[$val]){
    			$return_array = array(
    				'status'    => false,
    				'message' => $val.' field is required.',
    				'statusCode'=>'500'
    			);
    			echo json_encode($return_array,200); die;
    		}
    	}
        /*if(isset($parking_operative_data['created_time'])){
            $created_time = $parking_operative_data['created_time'];

            $start_date = date('Y-m-d 00:00:00', $created_time);
            $end_date = date('Y-m-d 23:59:59', $created_time);

            $where_customer['in_time >='] = $start_date;
            $where_customer['in_time <='] = $end_date;

        }*/
    	//fatching customer records from db using provided details
       /* $where_customer = array(
            'business_id'=>$parking_operative_data['business_id'],
        );*/


        $where_customer = '';
        $where_customer .= 'business_id = "'.$parking_operative_data['business_id'].'"';
        $and=' and ';

        if(isset($parking_operative_data['created_time'])){

            $created_time = $parking_operative_data['created_time'];
            $start_date = date('Y-m-d 00:00:00', $created_time);
            $end_date = date('Y-m-d 23:59:59', $created_time);

            $where_customer .= $and.'in_time >= "'.$start_date.'" and in_time <= "'.$end_date.'"';

        }

        if(isset($parking_operative_data['search_keyword'])){
            $searchData = trim($parking_operative_data['search_keyword']);
            $where_customer.= $and.'(customer_name like "%'.$searchData.'%" OR vehicle_registration_number like "%'.$searchData.'%" OR vehicle_registration_number like "%'.$searchData.'%" OR parking_status like "%'.$searchData.'%")';
        }

    	/*if(isset($parking_operative_data['search_keyword'])){
    		$search_keyword = $parking_operative_data['search_keyword'];
    		$or_like = array(
    			'customer_name'=>$search_keyword,
    			'contact_number'=>$search_keyword,
    			'vehicle_registration_number'=>$search_keyword,
    			'parking_status'=>$search_keyword
    		);
    	}*/
    	//Fetching customer records
    	$select = 'customer_name,contact_number,vehicle_registration_number,in_time,out_time,parking_status';
    	$customer_data = $this->api->getRecords($this->api->table_customer,$where_customer,$select);
        #echo $this->db->last_query(); die;
    	//output
    	if(!empty($customer_data)){
    		$return_array = array(
    			'status' => true,
    			'statusCode' => '200',
    			'message' => 'Customer List',
    			'customer_list' => $customer_data
    		);
    		echo json_encode($return_array, 200);
    		die;

    	}else{
    		$return_array = array(
    			'status'=>false,
    			'message'=>'No record found.',
    			'statusCode'=>'500'
    		);
    		echo json_encode($return_array,200); die;
    	}

    }


    /**
    * Change Password.
    * @return json encode array that contain flag,message
    * @param
    */
    function change_password_post(){

    	//$Required_arr: added all required fields
    	$required_arr = array('email','old_password','new_password','confirm_password');
    	$change_password=$this->post();

    	//Checking required fields key exists or not
    	foreach($required_arr as $key=>$val){
    		if(!array_key_exists($val, $this->post())){
    			$returnArray = array(
    				'status'    => false,
    				'message' => $val.' field is required.',
    				'statusCode'=>'500'
    			);
    			echo json_encode($returnArray,200); die;
    		}
    	}
         //Checking required fields value exists or not
    	foreach($required_arr as $key=>$val){
    		if(!$change_password[$val]){
    			$returnArray = array(
    				'status'    => false,
    				'message' => $val.' field is required.',
    				'statusCode'=>'500'
    			);
    			echo json_encode($returnArray,200); die;
    		}
    	}
    	$email = strtolower($change_password['email']);
    	$old_password = $change_password['old_password'];
    	$new_password = $change_password['new_password'];
    	$confirm_password = $change_password['confirm_password'];

       //Checking user status active/deactive
    	$this->check_user_status($email);

    	if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
    		$returnArray = array(
    			'status'=> false,
    			'message'=> 'Invalid email format',
    			'statusCode'=>'500'
    		);
    		echo json_encode($returnArray,200); die;
    	}

    	if ($new_password != $confirm_password) {
    		$returnArray = array(
    			'status'=> false,
    			'message'=> 'Confirm password not matched.',
    			'statusCode'=>'500'
    		);
    		echo json_encode($returnArray,200); die;
    	}

       //fetching salt to encrypting password
    	$where_salt = array(
    		'user_email'=>$email
    	);
    	$salt = $this->api->getSalt($this->api->table_users, $where_salt);

       //encrypting password to get records
    	$old_password = generateHash($old_password,$salt);
    	$where_user = array(
    		'user_email'=>$email,
    		'user_password'=>$old_password
    	);
    	$result = $this->api->checkRecord($this->api->table_users,$where_user);

    	if($result){

    		$where_user = array(
    			'user_email'=>$email
    		);
    		$save_data = array(
    			'user_password'=>generateHash($new_password)
    		);
    		$result = $this->api->save($this->api->table_users,$save_data,$where_user);

    		$returnArray = array(
    			'status'=>true,
    			'message'=>'Password changed successfully.',
    			'statusCode'=>'200'
    		);
    		echo json_encode($returnArray,200); die;
    	}else{
    		$returnArray = array(
    			'status'=>false,
    			'message'=>'Old password not matched.',
    			'statusCode'=>'500'
    		);
    		echo json_encode($returnArray,200); die;
    	}
    }

    /**
    * Forgot Password.
    * @return json encode array that contain flag,message
    * @param
    */
    function forgot_password_post(){

    	$forgot_password = $this->post();
    	$required_arr = array('email');

    	foreach($required_arr as $key=>$val){
    		if(!array_key_exists($val, $forgot_password )){
    			$returnArray = array(
    				'status'    => false,
    				'message' => $val.' field is required.',
    				'statusCode'=>'500'
    			);
    			echo json_encode($returnArray,200); die;
    		}
    	}
    	foreach($required_arr as $key=>$val){
    		if(!$forgot_password[$val]){
    			$returnArray = array(
    				'status'    => false,
    				'message' => $val.' field is required.',
    				'statusCode'=>'500'
    			);
    			echo json_encode($returnArray,200); die;
    		}
    	}

    	$email = strtolower($forgot_password['email']);

    	if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
    		$returnArray = array(
    			'status'=> false,
    			'message'=> 'Invalid email format',
    			'statusCode'=>'500'
    		);
    		echo json_encode($returnArray,200); die;
    	}

    	$this->check_user_status($email);

    	$where_user = array(
    		'user_email'=>$email,
    	);
    	$user_details = $this->api->getRecords($this->api->table_users,$where_user);

    	if(!empty($user_details)){

    		$auth_key  = mt_rand(100000, 999999);

    		$response['auth_key'] = $auth_key;

			$save_user_data = array(
				'auth_key' => $auth_key
			);
	        //save function is used to save or update records in a given table
			$last_id = $this->api->save($this->api->table_users,$save_user_data,$where_user);

    		if($last_id){
    			$mail_to      = $email;
    			$subject      = 'Reset Password!!';
    			$mail_extra   = '';
    			$mail_cc      = '';
    			$mail_bcc     = '';
    			if (!empty($mail_extra)) {
    				$mail_bcc = $mail_extra;
    			}

    			$sender_email = ADMIN_EMAIL;
    			$sender_name  = ADMIN_NAME;

    			$url = base_url().'reset_password/index/'. urlencode($auth_key);

    			$message_data = '<p>Dear '.$user_details[0]['user_name'].',</p>
    			<p>We have received your password request, please click on blow link to reset your password:</p>
    			<p><a href="'.$url.'">'.$url.'</p>';

    			$body           = array('email_content'=>$message_data);

    			$messagebody    = $this->parser->parse('emailer/email', $body, true);

    			$this->sendmail($email, $subject, $messagebody, $mail_cc, $mail_bcc, $sender_email, $sender_name, false);

    			$returnArray = array(
    				'status'=> true,
    				'message'=> 'We have sent you a email to reset password, Please check your email.',
    				'statusCode'=>'200'
    			);
    			echo json_encode($returnArray,200); die;
    		}
    	}else{
    		$returnArray = array(
    			'status'=>false,
    			'message'=>'No records found.',
    			'statusCode'=>'500'
    		);
    		echo json_encode($returnArray,200); die;
    	}
    }

    function check_user_status($email){
        //cheking user status
    	$where_user = array(
    		'user_email'=>$email,
    	);
    	$user_details = $this->api->getRecords($this->api->table_users,$where_user);

    	if(!empty($user_details)){
    		if($user_details[0]['user_is_deleted'] == '1'){
    			$returnArray = array(
    				'status'=>false,
    				'message'=>'Your account has been deleted by admin.',
    				'statusCode'=>'501'
    			);
    			echo json_encode($returnArray,200); die;
    		}elseif ($user_details[0]['user_is_active'] == '0') {
    			$returnArray = array(
    				'status'=>false,
    				'message'=>'Your account has been deactivated by admin.',
    				'statusCode'=>'501'
    			);
    			echo json_encode($returnArray,200); die;
    		}
    	}else{
    		$returnArray = array(
    			'status'    => false,
    			'message'    => 'Email not registerd with PCM',
    			'statusCode'=>'500'
    		);
    		echo json_encode($returnArray,200); die;
    	}
    }

    public function sendmail($to = '', $subject = '', $messagebody = '', $cc = array(), $bcc = array(), $from_email = '', $from_name = '',$attachment = ''){

    	$this->load->library('email');

    	$config = array();
    	$config['useragent']           = "CodeIgniter";
        $config['mailpath']            = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
        $config['protocol']            = "smtp";
        $config['_smtp_auth']          = TRUE;

        $config['smtp_host']           = 'usserver1.24livehost.com';
        $config['smtp_user']           = 'wwwsmtp@dotsquares.com';
        $config['smtp_pass']           = 'dsmtp909#';
        $config['smtp_port']           = 25;

        $config['mailtype']            = 'html';
        $config['charset']             = 'utf-8';
        $config['newline']             = "\r\n";
        $config['wordwrap']            = TRUE;

        $this->email->initialize($config);

        $this->email->set_mailtype("html");

        if(!empty($from_email) && !empty($from_name)){
        	$this->email->from($from_email, $from_name);
        }else{
        	$this->email->from(ADMIN_EMAIL, ADMIN_NAME);
        }


        if(!empty($to) && !is_array($to)){
        	$this->email->to($to);
        }

        if(!empty($cc)){

        	$this->email->cc($cc);
        }

        if(!empty($bcc) && !is_array($bcc)){
        	$bcc1 = array($bcc);
        	$bcc = NULL;
        	$bcc = $bcc1;

        }

        if(!empty($subject)){
        	$this->email->subject($subject);
        }

        if(!empty($messagebody)){
        	$this->email->message($messagebody);
        }


        if (!$this->email->send()){
           // echo 'No'; die;
        	return false;
        }else{
           // echo 'Yes'; die;
        	return true;
        }
        if(!empty($attachment)){
        	if(file_exists('./'.$attachment)){
        		$this->email->clear(TRUE);
        	}
        }

    }

    // Code done by Pawan Joshi
}
