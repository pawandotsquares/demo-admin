<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * this is a generic function to show html to home page
	 * function having no parameter
	 * @access public
	*/
	public function index()
	{
		redirect('admin');
	}
}
