<?php if ( ! defined('BASEPATH')) exit('No direct access allowed');

class Apis extends MY_Controller {


    /**
     * this is a generic function to show html for login to admin area
     * function having no parameter
     * function using template crm-login
     * function use to login area after successful field validation with captcha
     * function also check block ip address and login attemp before successful login to admin area
     * @access public
    */
    public function index(){

        $this->load->view('api/index');
    }

}
