<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Privilege extends MY_Controller {

	var $data      = array();
	var $error     = array();
	var $template  = array();
	var $middle     = '';
	var $navigation  = '';
	var $breadcrumbs    = '';
	var $sidebar = '';
	var $table_privileges = 'ps_privileges';

	public function __construct() {

		parent::__construct();
		$this->load->model(array('actions','controllers','privileges'));
		$this->admin_session_data = $this->session->userdata("admin_session_data");
	}
    /**
       * this is a generic function to show all role actions,active roles, all controllers
       * function having no parameter
       * function using action,roles, controller, privilege module with users module to whome role assigned
       * function set all privileges on view at location admin/action/index
       * @access public
    */
    public function index()
    {
    	$this->templates->set('admin-layout');
        $assigned_privileges = array();
    	$assigned_privileges = $this->privileges->getRecords($this->privileges->table,array(),'controller_id,role_id,action_id');

    	$role_privileges = array();
    	foreach($assigned_privileges as $assigns)
    	{
    		$role_privileges[$assigns['role_id']][] = $assigns['action_id'];
    	}
    	$middle['assigned_privileges'] = $role_privileges;
    	$middle['result'] = $this->actions->getRecords($this->actions->table,array(),'controller_id,action,action_url,alias');
        $where_role =array(
            'role_is_active' => '1',
            'role_is_deleted' => '0'
        );
    	$middle['groups'] = $this->privileges->getRecords($this->privileges->table_roles,$where_role);

    	$controllers = array();
    	$Pcontrollers = $this->controllers->getRecords($this->controllers->table,array(),'controller_id, controller_name, controller_url, controller_alias, ordering, controllers_is_active');

    	foreach($Pcontrollers as $controller)
    	{
    		$controllers[$controller['controller_id']] = $controller;
    		$controllers[$controller['controller_id']]['actions'] = $this->actions->get_all_action_by_controller($controller['controller_id']);
    	}

    	$middle['controllers'] = $controllers;

    	$middle['formTitle'] = 'Privilege Manager';

    	$middle['layout'] = 'admin/privilege/index';

    	$this->templates->set_data('content',$middle);

    	if( is_array( $this->input->post('privileges') ) && count($this->input->post('privileges')) > 0)
    	{

    		$this->db->query("TRUNCATE ".$this->table_privileges);
    		$privileges = $this->input->post('privileges');

    		$count_privilege = count($privileges);

    		$i = 1 ;
    		foreach($privileges as $role_id => $privilege )
    		{
    			foreach($privilege as $pri )
    			{
    				$data['role_id']  = $role_id ;
    				$controller_action = explode('_',$pri );

    				$data['controller_id'] = $controller_action[0];
    				$data['action_id'] = $controller_action[1];
    				if( (isset($data['role_id']) && !empty($data['role_id'])) && (isset($data['controller_id']) && !empty($data['controller_id'])) && (isset($data['action_id']) && !empty($data['action_id'])) )
    				{
                        $save_data = array(
                            'controller_id' => $data['controller_id'],
                            'role_id'  =>   $data['role_id'],
                            'action_id' =>  $data['action_id'],
                        );
    					$this->privileges->save($this->privileges->table, $save_data);
    					$this->session->set_flashdata('message','Privileges assigned successfully.');
    				}
    			}
    			if($i == $count_privilege){
    				redirect('admin/privilege/index');
    			}
    			$i ++;
    		}
    	}
    	$this->templates->load();
    }

	/**
	 * this is a generic function to add data about controller
	 * function having no parameter
	 * function using view to add controller
 	 * @access public
	*/

	public function add_controller()
	{
		$this->templates->set('admin-layout');
		$middle['layout'] = 'admin/privilege/add_controller';
		$middle['formTitle'] = 'Add Controller';
		$middle['buttonText'] = 'Add Controller';
		$this->templates->set_data('content',$middle);

		if( $this->input->post('controller_name') != '')
		{
			$this->controllers->add_controller($this->input->post('controller_name'),$this->input->post('alias'),$this->input->post('ordering'));
		}
		$this->templates->load();
	}

	/**
	 * this is a generic function to add data about action
	 * function having no parameter
	 * function using view to add action for controller
 	 * @access public
	*/

	public function add_action()
	{
		$this->templates->set('admin-layout');
		$middle['layout'] = 'admin/privilege/add_action';
		$middle['formTitle'] = 'Add Action';
		$middle['buttonText'] = 'Add Action';
		$middle['controllers'] = $this->controllers->getRecords($this->controllers->table,array(),'controller_id, controller_name,controller_url, controller_alias, ordering,controllers_is_active');

		if( $this->input->post('controller_id') != '' && $this->input->post('action') != '' && $this->input->post('alias') != '')
		{
			$this->actions->add_action($this->input->post('controller_id'),$this->input->post('action'),$this->input->post('alias'));
		}
		$this->templates->set_data('content',$middle);
		$this->templates->load();
	}
}
?>