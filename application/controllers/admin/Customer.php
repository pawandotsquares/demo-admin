<?php if ( ! defined('BASEPATH')) exit('No direct access allowed');

class Customer extends MY_Controller {

	function __construct(){
		parent::__construct();

		$this->load->model('users');
	}

    /**
     * this is a generic function to show html for Customer in admin area
     * function having no parameter
     * function using template admin-login
     * @access public
    */
    public function index(){
    	add_js(array('admin/js/sweetalert.min.js','admin/js/plugins/dataTables/jquery.dataTables.js','admin/js/plugins/dataTables/dataTables.bootstrap.js','admin/js/plugins/dataTables/dataTables.responsive.js','admin/js/plugins/dataTables/dataTables.tableTools.min.js'));
    	add_css(array('admin/css/plugins/dataTables/dataTables.bootstrap.css','admin/css/plugins/dataTables/dataTables.responsive.css','admin/css/plugins/dataTables/dataTables.tableTools.min.css','admin/css/sweetalert.css'));
    	$data = array();
    	$layout = 'admin-layout';
    	$view_file =  'admin/customer/index';
    	$index['page_title'] = ':: View Virtual Permit ::';
    	$content['form_title'] = 'View Virtual Permit';
        $where_business = array(
            'role_id' => '2',
            'user_is_active' => '1',
            'user_is_deleted' => '0'
        );
        //getting data of business user to display in customer section for filtration
        $content['business_data']           = $this->users->getRecords($this->users->table_users,$where_business,'user_id,user_name');
        $where_parking = array(
            'role_id' => '3',
            'user_is_active' => '1',
            'user_is_deleted' => '0'
        );
        //getting data of parking operative to display in customer section for filtration
        $content['parking_operative_data']  = $this->users->getRecords($this->users->table_users,$where_parking,'user_id,user_name');

      	$this->templates->set($layout);
    	$content['layout'] = $view_file;
    	$this->templates->set_data('index',$index);
    	$this->templates->set_data('content',$content);
    	$this->templates->load();
    }
    /**
     * this is a generic function to get records of customer
     * function having no parameter
     * function using template admin-login
     * @access public
    */
    public function get_customer_list(){

    	$order_by = array();
    	$length = $this->input->post('length');
    	$start = $this->input->post('start');
    	if(empty($length)){
    		$length = 10;
    		$start = 0;
    	}
    	$columnData = array(
            'sr_no',
    		'vehicle_registration_number',
    		'in_time',
            'business_id',
            'parking_officer_id'
    	);
    	$sortData = $this->input->post('order');
    	$order_by[0] = $columnData[$sortData[0]['column']];
    	$order_by[1] = $sortData[0]['dir'];
        $searchData = $this->input->post('searchBox');
        $dr_business_data    = $this->input->post('dr_business_data');
        $dr_parking_off_data = $this->input->post('dr_parking_off_data');
        $mindate = $this->input->post('mindate');
    	$maxdate = $this->input->post('maxdate');
        $where = '';
    	$where .= 'parking_status = "1"';
    	$and=' and ';
        //generation where condition according to data receiving from datatable
    	if($searchData){
    		$searchData = trim($searchData);
    		$where.= $and.'(contact_number like "%'.$searchData.'%" OR vehicle_registration_number like "%'.$searchData.'%" OR in_time like "%'.$searchData.'%" OR customer_name like "%'.$searchData.'%")';
    	}
        if($mindate && !$maxdate){
            $mindate1 = $mindate.' 00:00:00';
            $maxdate1 = $mindate.' 23:59:59';
            $where.= $and.'in_time >= "'.$mindate1.'" and in_time <= "'.$maxdate1.'"';
        }
        if($mindate && $maxdate){
            $maxdate = $maxdate.' 23:59:59';
            $where.= $and.'in_time >=  "'.$mindate.'" and in_time <= "'.$maxdate.'"';
        }
        if($dr_business_data){

            $where.= $and.'business_id =  '.$dr_business_data;
        }
        if($dr_parking_off_data){

            $where.= $and.'parking_officer_id =  '.$dr_parking_off_data;
        }

        $select = "customer_id,business_id,parking_officer_id,contact_number,vehicle_registration_number,in_time";

        //getting data of all the customer to dispaly in datatable in admin section
    	$customer_data = $this->users->getDataTableRecords($this->users->table_customer, $where, $select, $order_by, $start, $length,$where_in = false,$where_not_in = false);

    	$user_data = $customer_data['data'];
    	$total_data = $customer_data['total'];
    	$jsonArray=array(
    		'draw'=>$this->input->post('draw'),
    		'recordsTotal'=>$total_data,
    		'recordsFiltered'=>$total_data,
    		'data'=>array(),
    	);

    	foreach($user_data as $key => $val){

            $parking_operative_name = $this->users->getRecords($this->users->table_users,array('user_id'=>$val['parking_officer_id']),'user_name as parking_operative_name');
            $business_name = $this->users->getRecords($this->users->table_business,array('user_id'=>$val['business_id']),'business_name');

    		$jsonArray['data'][] = array(
    			'sr_no'           => $start + $key + 1,
                'vehicle_registration_number'  => $val['vehicle_registration_number']?$val['vehicle_registration_number']:'---',
    			'in_time'            => $val['in_time']?$val['in_time']:'---',
                'business_id'        => $business_name?$business_name[0]['business_name']:'---',
                'parking_officer_id' => $parking_operative_name?$parking_operative_name[0]['parking_operative_name']:'---',
    		);
    	}
    	echo json_encode($jsonArray); exit;
    }
    /**
     * this is a generic function to get records of parking operative
     * function having no parameter
     * function using template admin-login
     * @access public
    */
    public function get_business_list(){
        $where_parking = array(
            'B.parking_operative_id' => $this->input->post('parkingOperativeId'),
            'U.role_id' => '2',
            'U.user_is_active' => '1',
            'U.user_is_deleted' => '0'
        );
        //getting data of parking operative to display in customer section for filtration
        $business_data = $this->users->getBusinessData($where_parking, 'U.user_id,U.user_name');
        echo json_encode($business_data['data']); exit;
    }

}
?>