<?php
defined('BASEPATH') OR exit('No direct access allowed');

class Dashboard extends My_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('users');
	}
    /**
     * this is a generic function to show html to dashbord
     * function having no parameter
     * @access public
    */
    public function index(){

    	$role_title_array = array();
    	$data = array();
    	$layout = 'admin-layout';
    	$view_file =  'admin/dashboard';

    	$index['page_title'] = ':: Dashboard ::';

    	$content['form_title'] = 'Dashboard';

    	add_js(array('admin/js/loadingoverlay.min.js'));

    	$this->templates->set($layout);

    	$content['layout'] = $view_file;

        $where_parking_opertive = array(
            'role_id' => '3',
            'user_is_active' => '1',
            'user_is_deleted' => '0'
        );
        //getting count of customer to display in dashboard
        $parking_operative_data = $this->users->getRecords($this->users->table_users,$where_parking_opertive,'user_id');
        $content['operative_count'] = 0;
        if(!empty($parking_operative_data)){
            $content['operative_count'] = count($parking_operative_data);
        }

        //getting count of business to display in dashboard
        $where = 'U.user_is_deleted = "0" and U.user_is_active = "1" and U.role_id = 2';
        $business_data = $this->users->getBusinessData($where,'U.user_id');
        $content['business_count'] = 0;
        if(!empty($business_data)){
            $content['business_count'] = count($business_data['data']);
        }

        //getting count of customer to display in dashboard
        $customer_data = $this->users->getRecords($this->users->table_customer,null,'customer_id');
        $content['customer_count'] = 0;
        if(!empty($customer_data)){
            $content['customer_count'] = count($customer_data);
        }

    	$this->templates->set_data('index',$index);

    	$this->templates->set_data('content',$content);

    	$this->templates->load();
    }

}