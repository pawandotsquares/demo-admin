<?php if ( ! defined('BASEPATH')) exit('No direct access allowed');

class Login extends MY_Controller {

	function __construct(){
		parent::__construct();

		if($this->session->userdata('admin_session_data')){

			redirect(base_url('admin/dashboard'));
		}
		$this->load->model('users');
	}

    /**
     * this is a generic function to show html for login to admin area
     * function using template admin-login-layout
     * @access public
    */

    public function index(){

    	$this->templates->set('admin-login-layout');
    	$this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email');
    	$this->form_validation->set_rules('password', 'Password', 'required|trim');
    	if ($this->form_validation->run() == false)
    	{
    		$this->templates->load();
    	}
    	else{
    		$result = false;
    		$email      =   strtolower($this->input->post('email'));
    		$password   =   $this->input->post('password');

    		$where_salt = array(
    			'user_email'=>$email,
    			'user_is_active'=>'1',
    			'user_is_deleted'=>'0'
    		);
            //getting password salt using email id of active user
    		$salt_data = $this->users->getRecords($this->users->table_users, $where_salt);
            $salt = '';
            if (!empty($salt_data[0])) {
                $salt = $salt_data[0]['user_password'];
            }
            //creating password hash to make password same as we have in db
    		$password = generateHash($password,$salt);
    		$where_user = array(
    			'user_email'=>$email,
    			'user_password'=>$password,
    			'role_id'=>1
    		);
            //checking user records according to provided email and password
    		$result = $this->users->checkRecord($this->users->table_users,$where_user);

    		if($result){
                //cheking authentication
    			$this->_authenticate($email,$password);
    		}else{
    			$this->messages->add("Provided credentials does not match! Please try again!", "error");
    			redirect(base_url('admin'));
    		}
    	}
    }

    /**
     * this is a generic function to check for aunthentication to login
     * first parameter having information about user email
     * second parameter having information about password
     * third parameter having information if login information already remembered
     * @access public
     * @return boolean
    */

    public function _authenticate($user_email = '',$users_password = '',$login_remember = ''){
    	$res = false;
    	if(isset($login_remember) && $login_remember != ''){
    		setCookies($user_email,$users_password);
    	}
    	if(isset($user_email) && $user_email != '' && $users_password != '' && isset($users_password)){

    		$data = $this->users->getRecords($this->users->table_users, array('user_email' => $user_email,'user_password' => $users_password,'role_id'=>1));

    		if(!empty($data)){
    			$this->session->set_userdata('admin_session_data',$data[0]);
    			$redirect_url = base_url('admin');
    			redirect($redirect_url);
    		}else{
    			$this->messages->add("You do not have permission to access.", "error");
    			redirect(base_url('admin'));
    		}
    	}else{
    		$this->messages->add("You do not have permission to access.", "error");
    		redirect(base_url('admin'));
    	}

    }
}

?>