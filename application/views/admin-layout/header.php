<header class="main-header">
	<!-- Logo -->
	<a href="<?php echo base_url('admin/dashboard') ?>" class="logo">
		<!-- mini logo for sidebar mini 50x50 pixels -->
		<span class="logo-mini">
			AP
			<!-- <img src="<?php echo base_url();?>assets/smalllogo.png"/> -->
		</span>
		<!-- logo for regular state and mobile devices -->
		<span class="logo-lg"><b>Admin Panel </b></span>
	</a>
	<!-- Header Navbar: style can be found in header.less -->
	<nav class="navbar navbar-static-top" role="navigation">
		<!-- Sidebar toggle button-->
		<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
			<span class="sr-only">Toggle navigation</span>
		</a>
		<div class="navbar-custom-menu">
			<ul class="nav navbar-nav">
				<!-- Messages: style can be found in dropdown.less-->
				<?php $admin_session_data = $this->session->userdata("admin_session_data");?>
				<!-- User Account: style can be found in dropdown.less -->
				<li class="dropdown user user-menu">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="fa fa-user"></i>
						<span class="hidden-xs"></span>
					</a>
					<ul class="dropdown-menu">
						<!-- Menu Footer-->
						<li>
							<a href="<?php echo base_url('admin/change_password'); ?>" class="btn btn-default btn-flat">Change Password</a>
						</li>
						<li>
							<a href="<?php echo base_url('admin/logout');?>" class="btn btn-default btn-flat">Sign out</a>
						</li>
					</ul>
				</li>
				<!-- Control Sidebar Toggle Button -->
			</ul>
		</div>
	</nav>
</header>