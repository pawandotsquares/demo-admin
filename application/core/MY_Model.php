<?php
if ( ! defined('BASEPATH')) exit('No direct access allowed');

class MY_Model extends CI_Model {

	public function __construct() {
        parent::__construct();
    }

	public function getNextAutoIncreament($table) {

		$query = $this->db->query('SHOW TABLE STATUS LIKE \''.$this->db->dbprefix.$table.'\'');
		$last = $query->result_array();
		$last = $last[0]['Auto_increment'];
		return $last;
		$this->db->close();
	}

	/**
	 * this is a generic function to save data in db
	 * the 1st parameter is the table name
	 * the 2nd is an array with the data that will be saved
	 * the 3dt is optional. If you give an id the function will update the current id.
	 * Without id a new entry will add into db
	 *
 	 * @access public
	 * @param string
	 * @param array
	 * @param integer
	 * @return integer/boolean
	 */
	public function save($table,$columns = array(),$where = array())
	{
		if(empty($where) === false) {
			$this->db->where($where);
			$result = $this->db->update($table, $columns);
		} else{
			$result = $this->db->insert($table, $columns);
            $id = $this->db->insert_id();
            return $id;
		}

		if($result) {
			if(empty($where))
				return $this->db->insert_id();
			else
				return true;
		} else
			return false;
        $this->db->close();
	}


	/**
	 * this is a generic function to update data in db
	 * the 1st parameter is the table name
	 * the 2nd is an array with the data that will be saved
	 * the 3dt is an array that will used in where condition (AND).
	 * Without where all entry will be updated in selected table
	 *
 	 * @access public
	 * @param string
	 * @param array
	 * @param array
	 * @return integer/boolean
	 */
	public function updateRecord($table = NULL , $columns = NULL, $where = NULL)
	{

		if($table == NULL || $columns == NULL)
		{
			return;
		}

	   if($where){
		   $this->db->where($where);
	   }

	   $result = $this->db->update($table, $columns);
	   return $result;
		$this->db->close();
	}

	public function checkCardsRecord($table, $where = array())
	{
		$this->db->select('*');

		$this->db->where($where);

		$query = $this->db->get($table);

		if($query->num_rows() > 0 )
		{
			return $query->result_array();

		}
		else{

			return false ;
		}
        $this->db->close();
	}
	function getSalt($table, $where = '') {

	    $this->db->from($table);

	    if(!empty($where)){
	        $this->db->where($where);
	    }

	    $data = $this->db->get()->result_array();
	    $user_password = '';
	    if (!empty($data[0])) {
	        $user_password = $data[0]['user_password'];
	    }
	    return $user_password;
	}

    function getDataTableRecords($table,$where = NULL,$select =  '',$order_by= NULL, $offset=0, $limit=10,$where_in = false,$where_not_in = false){

        $this->db->from($table);

        if(!empty($where)){
            $this->db->where($where);
        }
        if($where_in){
            $this->db->where_in($where_in[0], $where_in[1]);
        }
        if($where_not_in){
            $this->db->where_not_in($where_not_in[0], $where_not_in[1]);
        }
        if(!empty($order_by)){
           $this->db->order_by($order_by[0],$order_by[1]);
		}
        $result=  array();
        if($limit){
            $tempdb 		 = clone $this->db;
			$totaldata 		 = $tempdb->get();
			$result['total'] = $totaldata->result_id->num_rows;
            $this->db->limit($limit, $offset);
        }
        $result['data'] =  $this->db->get()->result_array();
        return $result;
	}

	/**
	 * this is a generic function to get data from db
	 * the 1st parameter is the table name
	 * the 2nd is an array that will used in where condition (AND)
	 * the 3rd is order by an array.array('id','DESC');
	 * the 4th and 5th is num and Limit that is using for Limit
 	 * @access public
	 * @param string
	 * @param array
	 * @return array
	 */
 public function getRecords($table, $where = NULL,$select = null, $where_or = NULL, $or_like = NULL,$order_by=NULL, $num = NULL, $offset = NULL, $where_in = NULL, $where_not_in = null)
	{

        if(!empty($select)){
	       $this->db->select($select,false);
	    }else{
	       $this->db->select('*');
	    }

        if($where != NULL )
		{
		 	$this->db->where($where);
		}
        if($where_in != NULL && isset($where_in[0]) && isset($where_in[1])){
            $this->db->where_in($where_in[0],$where_in[1]);
        }

        if($where_not_in != NULL && isset($where_not_in[0]) && isset($where_not_in[1]) && !empty($where_not_in[1])){
            $this->db->where_not_in($where_not_in[0],$where_not_in[1]);
        }

        if($where_or != NULL){
            $this->db->or_where($where_or);
        }

        if($or_like != NULL){
            $this->db->or_like($or_like);
        }

		if($num != NULL || $offset != NULL)
		{
			$this->db->limit($num, $offset);
		}

		if($order_by)
		{
		   $this->db->order_by($order_by[0], $order_by[1]);
		}

		$query = $this->db->get($table);

		if($query->num_rows() > 0 )
		{
			$ret = $query->result_array();
			$query->free_result();
			return $ret;

		}else{
			$ret = array();
			$query->free_result();
			return $ret;
		}

        $this->db->close();
	}
 public function getRecords1($table, $where = NULL,$order_by=NULL, $select = null, $where_or = NULL, $or_like = NULL, $num = NULL, $offset = NULL, $where_in = NULL, $where_not_in = null)
	{

        if(!empty($select)){
	       $this->db->select($select,false);
	    }else{
	       $this->db->select('*');
	    }

        if($where != NULL )
		{
		 	$this->db->where($where);
		}
        if($where_in != NULL && isset($where_in[0]) && isset($where_in[1])){
            $this->db->where_in($where_in[0],$where_in[1]);
        }

        if($where_not_in != NULL && isset($where_not_in[0]) && isset($where_not_in[1]) && !empty($where_not_in[1])){
            $this->db->where_not_in($where_not_in[0],$where_not_in[1]);
        }

        if($where_or != NULL){
            $this->db->or_where($where_or);
        }

        if($or_like != NULL){
            $this->db->or_like($or_like);
        }

		if($num != NULL || $offset != NULL)
		{
			$this->db->limit($num, $offset);
		}

		if($order_by)
		{
		   $this->db->order_by($order_by[0], $order_by[1]);
		}

		$query = $this->db->get($table);

		if($query->num_rows() > 0 )
		{
			$ret = $query->result_array();
			$query->free_result();
			return $ret;

		}else{
			$ret = array();
			$query->free_result();
			return $ret;
		}

        $this->db->close();
	}
    /**
     * this is a generic function to detele data from db
     * the 1st parameter is the table name
     * the 2nd is an array that will used in where condition (AND)
     * @access public
     * @param array
     * @return integer/boolean
     */
    function getData($table = NULL, $where = NULL,$select =  '',$order_by= NULL, $offset=0, $limit=10,$where_in = false,$where_not_in = false){

        $this->db->from($table);

        if(!empty($where)){
            $this->db->where($where);
        }
        if($where_not_in){
            $this->db->where_not_in($where_not_in[0], $where_not_in[1]);
        }
        if(!empty($order_by)){
           $this->db->order_by($order_by[0],$order_by[1]);
        }
        $result=array();
        if($limit){
            $tempdb          = clone $this->db;
            $totaldata       = $tempdb->get();
            $result['total']=$totaldata->result_id->num_rows();
            $this->db->limit($limit, $offset);
        }
        $result['data'] =  $this->db->get()->result_array();

        return $result;

    }

	/**
	 * this is a generic function to detele data from db
	 * the 1st parameter is the table name
	 * the 2nd is an array that will used in where condition (AND)
 	 * @access public
	 * @param array
	 * @return integer/boolean
	 */

	public function deleteRecord($table, $where = array())
	{
		$this->db->where($where);

		$res = $this->db->delete($table);

		if($res)
			return TRUE;
		else
			return FALSE;
        $this->db->close();
	}


	/**
	 * this is a generic function to check any records in db
	 * the 1st parameter is the table name
	 * the 2nd is an array that will used in where condition (AND)
 	 * @access public
	 * @param array
	 * @return integer/boolean
	 */
	public function checkRecord($table, $where = array())
	{
		$this->db->select('*');

		$this->db->where($where);

		$query = $this->db->get($table);

		if($query->num_rows() > 0 )
		{
			return true ;

		}else{

			return false ;
		}
        $this->db->close();
	}



	/**
	 * this is a generic function to send email to all
	 * the 1st parameter array of emails
	 * the 2nd parameter is a string that will used as Subject
	 * the 3rd parameter is a string(Either HTML or text) that will used as email body
	 * the 4th parameter is a string( Name of user to whome email will be sent)
	 * the 5th parameter is a string used as cc
	 * the 6th parameter is a string used as bcc
 	 * @access public
	 * @param Array/string
	 * @return integer/boolean
	 */
	public function sendEmailAll($to=array(),$subject,$body,$name='',$cc=array(),$bcc =array())
	{
		$mail = new PHPMailer;
		if(count($to) > 0){
			foreach($to as $v){
			  if($v)
			  {
				$mail->AddAddress($v, $v);
			  }
			}

		}else{

			$mail->AddAddress($to, $name);
		}

		$mail->From		= $this->config->item('admin_email_from');
		$mail->FromName = $this->config->item('admin_email_from');
		$mail->Subject 	=  $subject;

		if(!empty($cc)){
			foreach($cc as $cv){
			  if($cv)
			  {
				$mail->AddCC($cv);
			  }
			}
		}

		if(!empty($bcc)){
			foreach($bcc as $bccv){
			  if($bccv)
			  {
				$mail->AddBCC($bccv);
			  }
			}
		}

		$mail->IsSMTP();
		$mail->Host = "mail.dotsquares.com";
		$mail->SMTPAuth = true;
		$mail->IsMail();
		$mail->WordWrap = 200;
		$mail->IsHTML(true);
		$mail->Body  = $body;

		if($mail->Send())
		{
			return true;
		}else{
			return false;
		}
	   $this->db->close();
	}


	public function fetchAll($return = 'list' , $table="",  $where = array(), $key = 'id', $value = 'title', $empty = false) {

		if($table != '') {
		$this->db->select('*');
		if(!empty($where)) {
			$this->db->where($where);
		}
		$this->db->order_by($value, 'ASC');
		$query = $this->db->get($table);

		if($return == 'list') {

			return MY_Model::generateFormatedList($query->result_array(), $key, $value, $empty) ;

		} else {

			return $query->result_array();

		}

		} else {
			return array();
		}

		$this->db->close();

	}

	//Fetch all rows from table.
	 public function fetch_all_rows($table="",  $where = array()) {
		if($table != '') {
			$this->db->select('*');
			if(!empty($where)) {
				$this->db->where($where);
			}
			$query = $this->db->get($table);
			return $query->result_array();

		}
		else
		{
			return array();
		}

        $this->db->close();
	}

	/**
	 * this is a generic function to save data in db as batch
	 * the 1st parameter is the table name
	 * the 2nd is an array with the data that will be saved
	 * the 3dt is optional field name. If you give an id the function will update the current column value.
	 *
 	 * @access public
	 * @param string
	 * @param array
	 * @param integer
	 * @return integer/boolean
	 */
	public function save_batch($table,$columns = array(),$field = NULL)
	{
		$this->db->cache_delete_all();

		if($field) {
			$result = $this->db->update_batch($table, $columns, $field);
		} else{

			$result = $this->db->insert_batch($table, $columns);
            $id = $this->db->insert_id();
            return $id;
		}

		if($result) {
				return TRUE;
		} else
			return FALSE;
        $this->db->close();
	}

	//Fetch all rows from table.
	 public function fetch_config_rows($value='') {
	    $this->db->from('config AS A');
		if($value != '') {
			$this->db->where('A.config_namekey', $value);
			$query = $this->db->get();
			$arr = $query->result_array();
			return $arr[0]['config_value'];

		}
		else
		{
			return array();
		}
        $this->db->close();
	}
	/**
	 * this is a generic function to count record in any table
	 * the 1st parameter is the table name
	 * @access public
	 * @param string
	 * @return integer/boolean
	 */
	public function countAllRecords($table_name){
		return $this->db->count_all($table_name);
        $this->db->close();
	}

}