-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 13, 2019 at 06:38 AM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `demo_admin`
--

-- --------------------------------------------------------

--
-- Table structure for table `ps_actions`
--

CREATE TABLE `ps_actions` (
  `id` int(11) NOT NULL,
  `controller_id` int(11) NOT NULL,
  `action` varchar(255) NOT NULL,
  `action_url` varchar(255) DEFAULT '',
  `alias` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `ps_actions`
--

INSERT INTO `ps_actions` (`id`, `controller_id`, `action`, `action_url`, `alias`) VALUES
(1, 1, 'add', '/add', 'Add Parking Operative'),
(2, 1, 'index', '/index', 'View Parking Operative'),
(3, 2, 'index', '/index', 'View Business'),
(4, 3, 'index', '/index', 'View Virtual Permit'),
(5, 4, 'index', '/index', 'View Dashboard');

-- --------------------------------------------------------

--
-- Table structure for table `ps_assign_operative`
--

CREATE TABLE `ps_assign_operative` (
  `assign_id` int(11) NOT NULL,
  `business_id` int(11) NOT NULL,
  `parking_operative_id` int(11) NOT NULL,
  `created_time` datetime NOT NULL,
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `ps_business`
--

CREATE TABLE `ps_business` (
  `business_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `parking_operative_id` int(11) NOT NULL,
  `business_name` varchar(255) NOT NULL,
  `business_type` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `is_active` enum('0','1') NOT NULL DEFAULT '0',
  `is_deleted` enum('0','1') NOT NULL DEFAULT '0',
  `created_time` datetime NOT NULL,
  `updated_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ps_controllers`
--

CREATE TABLE `ps_controllers` (
  `controller_id` int(11) NOT NULL,
  `controller_name` varchar(255) NOT NULL,
  `controller_url` varchar(255) DEFAULT NULL,
  `controller_alias` varchar(100) NOT NULL,
  `ordering` int(11) NOT NULL,
  `controllers_is_active` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `ps_controllers`
--

INSERT INTO `ps_controllers` (`controller_id`, `controller_name`, `controller_url`, `controller_alias`, `ordering`, `controllers_is_active`) VALUES
(1, 'Parking_operative', '/Parking_operative', 'Manage Parking Operative', 2, '0'),
(2, 'business', '/business', 'Manage Business ', 3, '0'),
(3, 'customer', '/customer', 'Manage Virtual Permit', 4, '0'),
(4, 'dashboard', '/dashboard', 'Dashboard', 1, '0');

-- --------------------------------------------------------

--
-- Table structure for table `ps_customer`
--

CREATE TABLE `ps_customer` (
  `customer_id` int(11) NOT NULL,
  `business_id` int(11) NOT NULL,
  `parking_officer_id` int(11) NOT NULL,
  `customer_name` varchar(255) NOT NULL,
  `contact_number` varchar(255) NOT NULL,
  `vehicle_registration_number` varchar(255) NOT NULL,
  `in_time` datetime NOT NULL,
  `out_time` datetime NOT NULL,
  `parking_status` int(11) NOT NULL DEFAULT '1' COMMENT '1 for parked, 2 for left',
  `created_time` datetime NOT NULL,
  `updated_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ps_privileges`
--

CREATE TABLE `ps_privileges` (
  `id` int(11) NOT NULL,
  `controller_id` int(11) NOT NULL DEFAULT '0',
  `role_id` int(11) NOT NULL DEFAULT '0',
  `action_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `ps_privileges`
--

INSERT INTO `ps_privileges` (`id`, `controller_id`, `role_id`, `action_id`) VALUES
(1, 1, 1, 1),
(2, 1, 1, 2),
(3, 2, 1, 3),
(4, 3, 1, 4),
(5, 4, 1, 5);

-- --------------------------------------------------------

--
-- Table structure for table `ps_roles`
--

CREATE TABLE `ps_roles` (
  `role_id` int(11) NOT NULL,
  `role_description` varchar(255) NOT NULL,
  `role_title` varchar(255) NOT NULL,
  `role_is_active` tinyint(4) NOT NULL DEFAULT '1',
  `role_is_deleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `ps_roles`
--

INSERT INTO `ps_roles` (`role_id`, `role_description`, `role_title`, `role_is_active`, `role_is_deleted`) VALUES
(1, '', 'Administrator ', 1, 0),
(2, '', 'Business', 1, 0),
(3, '', 'Parking Operative', 1, 0),
(4, '', 'Customer', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ps_security`
--

CREATE TABLE `ps_security` (
  `id` int(10) NOT NULL,
  `apikey` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ps_security`
--

INSERT INTO `ps_security` (`id`, `apikey`) VALUES
(1, 'PSsecruity2018');

-- --------------------------------------------------------

--
-- Table structure for table `ps_users`
--

CREATE TABLE `ps_users` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `contact_number` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `auth_key` varchar(255) NOT NULL,
  `user_is_active` enum('1','0') NOT NULL DEFAULT '1',
  `user_is_deleted` enum('0','1') NOT NULL DEFAULT '0',
  `created_time` datetime NOT NULL,
  `updated_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `ps_users`
--

INSERT INTO `ps_users` (`user_id`, `role_id`, `user_name`, `user_email`, `contact_number`, `user_password`, `auth_key`, `user_is_active`, `user_is_deleted`, `created_time`, `updated_time`) VALUES
(1, 1, 'admin', 'admin@demo.com', '', '2faa6378c3217a5a4ffd3932ac5f47c86e236c0e4205a34c892e7742062e7603c', '', '1', '0', '2018-05-25 11:18:44', '2018-05-25 05:48:44');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ps_actions`
--
ALTER TABLE `ps_actions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `controller_id` (`controller_id`);

--
-- Indexes for table `ps_assign_operative`
--
ALTER TABLE `ps_assign_operative`
  ADD PRIMARY KEY (`assign_id`),
  ADD KEY `business_id` (`business_id`),
  ADD KEY `officer_id` (`parking_operative_id`);

--
-- Indexes for table `ps_business`
--
ALTER TABLE `ps_business`
  ADD PRIMARY KEY (`business_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `ps_controllers`
--
ALTER TABLE `ps_controllers`
  ADD PRIMARY KEY (`controller_id`);

--
-- Indexes for table `ps_customer`
--
ALTER TABLE `ps_customer`
  ADD PRIMARY KEY (`customer_id`),
  ADD KEY `business_id` (`business_id`),
  ADD KEY `parking_officer_id` (`parking_officer_id`);

--
-- Indexes for table `ps_privileges`
--
ALTER TABLE `ps_privileges`
  ADD PRIMARY KEY (`id`),
  ADD KEY `controller_id` (`controller_id`),
  ADD KEY `role_id` (`role_id`),
  ADD KEY `action_id` (`action_id`);

--
-- Indexes for table `ps_roles`
--
ALTER TABLE `ps_roles`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `ps_security`
--
ALTER TABLE `ps_security`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ps_users`
--
ALTER TABLE `ps_users`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `role_id` (`role_id`),
  ADD KEY `user_id_2` (`user_id`),
  ADD KEY `role_id_2` (`role_id`),
  ADD KEY `user_id_3` (`user_id`),
  ADD KEY `role_id_3` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ps_actions`
--
ALTER TABLE `ps_actions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `ps_assign_operative`
--
ALTER TABLE `ps_assign_operative`
  MODIFY `assign_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `ps_business`
--
ALTER TABLE `ps_business`
  MODIFY `business_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `ps_controllers`
--
ALTER TABLE `ps_controllers`
  MODIFY `controller_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `ps_customer`
--
ALTER TABLE `ps_customer`
  MODIFY `customer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=93;
--
-- AUTO_INCREMENT for table `ps_privileges`
--
ALTER TABLE `ps_privileges`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `ps_roles`
--
ALTER TABLE `ps_roles`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `ps_security`
--
ALTER TABLE `ps_security`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ps_users`
--
ALTER TABLE `ps_users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `ps_actions`
--
ALTER TABLE `ps_actions`
  ADD CONSTRAINT `ps_actions_ibfk_1` FOREIGN KEY (`controller_id`) REFERENCES `ps_controllers` (`controller_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `ps_assign_operative`
--
ALTER TABLE `ps_assign_operative`
  ADD CONSTRAINT `assign_business_user_id_customer_id` FOREIGN KEY (`business_id`) REFERENCES `ps_users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `assign_officer_id_customer_id` FOREIGN KEY (`parking_operative_id`) REFERENCES `ps_users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ps_business`
--
ALTER TABLE `ps_business`
  ADD CONSTRAINT `business_id_user_table_user_id` FOREIGN KEY (`user_id`) REFERENCES `ps_users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ps_customer`
--
ALTER TABLE `ps_customer`
  ADD CONSTRAINT `add_business_user_id_customer_id` FOREIGN KEY (`business_id`) REFERENCES `ps_business` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `add_officer_user_id_customer_id` FOREIGN KEY (`parking_officer_id`) REFERENCES `ps_users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ps_users`
--
ALTER TABLE `ps_users`
  ADD CONSTRAINT `role_table_role_id_user_role_id` FOREIGN KEY (`role_id`) REFERENCES `ps_roles` (`role_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
