<?php
defined('BASEPATH') OR exit('No direct access allowed');

class Order extends MY_Controller {
    var $userData   = array();
    public function __construct() {
        parent::__construct();
        if($this->session->userdata("vendor_id")){
            $this->messages->add('No direct script access allowed.','error');
            redirect('vendor/dashboard');
        }
        
        $this->load->model('product_model', 'product');
        $this->load->model('users');
        $this->load->helper('custom');
        $this->load->library('Pdf');
        $this->load->library('session');

    }

public function pdfgen(){

    add_css(array('admin/css/plugins/dataTables/dataTables.bootstrap.css','admin/css/plugins/dataTables/dataTables.responsive.css','admin/css/plugins/dataTables/dataTables.tableTools.min.css','admin/css/sweetalert.css','admin/css/sweetalert.css','admin/css/plugins/daterangepicker/daterangepicker-bs3.css','admin/css/plugins/datapicker/datepicker3.css'));
    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    // set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('');
    $pdf->SetTitle('Order Deatil');
    $pdf->SetSubject('');
    $pdf->SetKeywords('');

   
    $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(false);

    // set margins
    
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

    // set auto page breaks
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

    // set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

    // set some language-dependent strings (optional)
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
        require_once(dirname(__FILE__).'/lang/eng.php');
        $pdf->setLanguageArray($l);
    }

    // set default font subsetting mode
    $pdf->setFontSubsetting(true);

    // Set font
    $pdf->SetFont('dejavusans', '', 14, '', true);

    // Add a page
    // This method has several options, check the source code documentation for more information.
    $pdf->AddPage();

    // set text shadow effect
    $pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

    // Set some content to print
     $searchData = $this->session->userdata('searchData');
     $userdata = $this->session->userdata('userdata');
     $usertype = $this->session->userdata('usertype');
     $orderstatustype = $this->session->userdata('orderstatustype');
     $paymentstatustype = $this->session->userdata('paymentstatustype');
     $start_date = $this->session->userdata('start_date');
     $end_date = $this->session->userdata('end_date');


     $where = '';
        $and=' and ';
         if($searchData){
            $searchData = trim($searchData);

            $where.= '(O.created_at like "%'.$searchData.'%" OR O.order_status like "%'.$searchData.'%" OR O.order_uuid like "%'.$searchData.'%" OR O.quantity LIKE "'.$searchData.'" OR PM.id like "%'.$searchData.'%" OR CONCAT(U.first_name,"",U.last_name) LIKE "'.$searchData.'")';

        }

        if($usertype){
            if($searchData){
                $where.= $and.'U.role_id =  '.$usertype;
            }else{
                $where.='U.role_id =  '.$usertype;
            }
        }


        if($userdata){
            if($searchData || $usertype){
                $where.= $and.'U.id =  '.$userdata;
            }else{
                $where.='U.id =  '.$userdata;
            }
        }
        //echo $orderstatustype;
        if(is_numeric($orderstatustype)){
            if($searchData || $usertype || $userdata){
                $where.= $and.'O.order_status =  '.$orderstatustype;
            }else{
                $where.='O.order_status =  '.$orderstatustype;
            }
        } //die;

         if(is_numeric($paymentstatustype)){
            if($searchData || $usertype || $userdata || $orderstatustype){
                $where.= $and.'PM.payment_status =  '.$paymentstatustype;
            }else{
                $where.='PM.payment_status =  '.$paymentstatustype;
            }
        }

      
        if((date("Y-m-d", strtotime($this->input->post('start_date')) )!='1970-01-01')&&(date("Y-m-d", strtotime($this->input->post('end_date')) )!='1970-01-01')){

                $sd = "'$start_date'";
                $ed = "'$end_date'";
               
                if($searchData || $usertype || $orderstatustype || $paymentstatustype){
                   $where.= $and.'O.created_at >= ' .$sd.' and O.created_at <= '.$ed;

                }else{
                    $where.= 'O.created_at >= '.$sd.' and O.created_at <= '.$ed;
                }
        }



    $order_by = array();
    $length = 0;
    $start = 0;
    $order_by[0] = 'first_name';
    $order_by[1] = 'ASC';

    $List_Array = $this->product->getOrderPaymentData($where,$select =  'U.role_id,U.first_name,U.last_name,O.*,PM.*,PM.id as payId',$order_by, $start, $length,$where_in = false,$where_not_in = false,'PM.order_uuid');

    $orderList = $List_Array['data'];

    ob_end_clean();
    $pdf_data_array = array(
        'orderList' => $orderList,
    );
    // Print text using writeHTMLCell()
    $html = $this->load->view('admin/pdf/index',$pdf_data_array,true);
    $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);


    $pdf->Output('example_001.pdf', 'I');

}


    public function index(){

        add_js(array('admin/js/sweetalert.min.js','admin/js/plugins/dataTables/jquery.dataTables.js','admin/js/plugins/dataTables/dataTables.bootstrap.js','admin/js/plugins/dataTables/dataTables.responsive.js','admin/js/plugins/dataTables/dataTables.tableTools.min.js','admin/js/plugins/daterangepicker/daterangepicker.js','admin/js/plugins/datapicker/bootstrap-datepicker.js','admin/js/plugins/dataTables/dataTables.buttons.min.js','admin/js/plugins/dataTables/pdfmake.min.js','admin/js/plugins/dataTables/vfs_fonts.js','admin/js/plugins/dataTables/buttons.html5.min.js'));


        add_css(array('admin/css/plugins/dataTables/dataTables.bootstrap.css','admin/css/plugins/dataTables/dataTables.responsive.css','admin/css/plugins/dataTables/dataTables.tableTools.min.css','admin/css/sweetalert.css','admin/css/sweetalert.css','admin/css/plugins/daterangepicker/daterangepicker-bs3.css','admin/css/plugins/datapicker/datepicker3.css'));

        $data = array();
        $layout = 'admin-layout';
        $view_file =  'admin/orders/index';
        $index['page_title'] = ':: View Orders ::';
        $content['form_title'] = 'View Orders';

        $start_date= date('d-m-Y') ;
        $end_date= date('d-m-Y');

        $where_key = array(
            'role_id !='=>1,
            'is_active'=>1,
            'is_deleted'=>0,
        );
        $order_by[0] = 'first_name';
        $order_by[1] = 'ASC';
        $user_details = $this->users->getRecords($this->users->table_users,$where_key,'id,first_name,last_name,email',null,null,$order_by);
        $content['users'] = $user_details;

        $this->templates->set($layout);
        $content['layout'] = $view_file;
        $this->templates->set_data('index',$index);
        $this->templates->set_data('content',$content);
        $this->templates->load();
    }

    public function view($id){
        if($id == ''){
            $this->messages->add('No direct script access allowed.','error');
            redirect('admin/order/index');
        }
        $data = array();
        $index['page_title'] = ':: Order Details ::';
        $content['form_title'] = 'Order Details';
        $layout = 'admin-layout';
        $view_file =  'admin/orders/view';

        $product_size_data = $this->product->getRecords($this->product->table_product_size);

        foreach ($product_size_data as $key => $value) {

            $productsize[$key] = $value['size_title'];
        }
        $content['productsize'] = $productsize;

        $where = array('O.order_uuid' => $id);
        $data = $this->product->getOrderPaymentData($where,$select =  'P.product_name, P.sku, PM.wallet_amount_used,P.image,U.first_name,U.last_name,U.role_id, U.email, U.phone_number, U.address, O.*,PM.amount as final_amount,PM.payment_status,D.*, PM.mode_of_payment,PM.id as Payment_id');
       
        if(empty($data['data'])){
          $this->messages->add('No records found.','error');

         redirect('admin/order/index');
        }else{
            $content['order_data'] = $data['data'];
        }

       
        $this->templates->set($layout);
        $content['layout'] = $view_file;
        $this->templates->set_data('index',$index);
        $this->templates->set_data('content',$content);
        $this->templates->load();
    }
    public function get_orders(){
        
        $order_by = array();
        $length = $this->input->post('length');
        $start = $this->input->post('start');

        
        if(empty($length)){
            $length = 10;
            $start = 0;
        }
        $columnData = array(
            'sr_no',
            'PM.order_uuid',
            'U.id',
            'U.role_id',
            'PM.mode_of_payment',
            'PM.id',
            'PM.amount',
            'PM.payment_status',
            'O.order_status',
            'PM.created_at',
            'action'
        );
        $sortData = $this->input->post('order');
        $order_by[0] = $columnData[$sortData[0]['column']];
        $order_by[1] = $sortData[0]['dir'];
        $searchData = $this->input->post('searchBox');
        $userdata = $this->input->post('userdata');
        $usertype = $this->input->post('usertype');
        $orderstatustype = $this->input->post('orderstatustype');
        $paymentstatustype = $this->input->post('paymentstatustype');

        $start_date= date("Y-m-d", strtotime($this->input->post('start_date')) );
        $end_date= date("Y-m-d", strtotime($this->input->post('end_date')) );


        $this->session->set_userdata(array(
            'searchData'  => $searchData,
            'userdata' => $userdata,
            'usertype'  => $usertype,
            'orderstatustype' => $orderstatustype,
            'paymentstatustype' => $paymentstatustype,
            'start_date'   => $start_date,
            'end_date'   => $end_date

        ));



        $where = '';
        $and=' and ';
         if($searchData){
            $searchData = trim($searchData);

            $where.= '(O.created_at like "%'.$searchData.'%" OR O.order_status like "%'.$searchData.'%" OR O.order_uuid like "%'.$searchData.'%" OR O.quantity LIKE "'.$searchData.'" OR PM.id like "%'.$searchData.'%" OR CONCAT(U.first_name,"",U.last_name) LIKE "'.$searchData.'")';

        }

        if($usertype){
            if($searchData){
                $where.= $and.'U.role_id =  '.$usertype;
            }else{
                $where.='U.role_id =  '.$usertype;
            }
        }


        if($userdata){
            if($searchData || $usertype){
                $where.= $and.'U.id =  '.$userdata;
            }else{
                $where.='U.id =  '.$userdata;
            }
        }
        
        if(is_numeric($orderstatustype)){
            if($searchData || $usertype || $userdata){
                $where.= $and.'O.order_status =  '.$orderstatustype;
            }else{
                $where.='O.order_status =  '.$orderstatustype;
            }
        } 

        
         if(is_numeric($paymentstatustype)){
            if($searchData || $usertype || $userdata || (!empty($orderstatustype) || ($orderstatustype==0 && $orderstatustype!=''))){
                $where.= $and.'PM.payment_status =  '.$paymentstatustype;
            }else{
                $where.= 'PM.payment_status =  '.$paymentstatustype;
            }
        }
        
       
        if((date("Y-m-d", strtotime($this->input->post('start_date')) )!='1970-01-01')&&(date("Y-m-d", strtotime($this->input->post('end_date')) )!='1970-01-01')){

                $sd = "'$start_date'";
                $ed = "'$end_date'";
                if($searchData || $usertype || $orderstatustype || $paymentstatustype){
                    $where.= $and.'O.created_at >= ' .$sd.' and O.created_at <= '.$ed;

                }else{
                    $where.= 'O.created_at >= '.$sd.' and O.created_at <= '.$ed;
                }
        }


        $List_Array = $this->product->getOrderPaymentData($where,$select =  'U.role_id,U.first_name,U.last_name,O.*,PM.*,PM.id as payId',$order_by, $start, $length,$where_in = false,$where_not_in = false,'PM.order_uuid');

        $orderList = $List_Array['data'];
        $totalData = $List_Array['total'];

        $jsonArray=array(
            'draw'=>$this->input->post('draw'),
            'recordsTotal'=>$totalData,
            'recordsFiltered'=>$totalData,
            'data'=>array(),
        );

        
        foreach($orderList as $key => $val){

            $view = '<a href="'.base_url('admin/order/view/'.$val['order_uuid']).'" class="tableIcon"><i class="fa fa-eye" title="View"></i></a>';
            $ship = '<a href="'.base_url('admin/order/ship/'.$val['order_uuid']).'" class="tableIcon"><i class="fa fa-shopping-cart" title="Shipping"></i></a>';

            $full_name = ($val['first_name'])?ucfirst($val['first_name']." ".$val['last_name']):'---';
            $shipStatus = 0;

            if($val['order_status'] == 3){
                $ship = '<a href="'.base_url('admin/order/track_order/'.$val['order_uuid']).'" class="tableIcon"><i class="fa fa-truck" title="Tracking"></i></a>';
            }

            if($val['payment_status'] == 0){
                $paymentStatus = "Failed";

            }elseif ($val['payment_status'] == 1) {
                $paymentStatus = "Pending";
            }elseif ($val['payment_status'] == 2) {
                $paymentStatus = "Paid";
                $shipStatus = 1;
            }else{
                $paymentStatus = "Pending";
            }

            if($val['order_status'] == 0){
                $order_status = "Failed";
            }elseif ($val['order_status'] == 1) {
                $order_status = "Processing";
            }elseif ($val['order_status'] == 2) {
                $order_status = "Placed";
            }elseif ($val['order_status'] == 3) {
                $order_status = "Dispatched";
                $shipStatus = 1;
            }else{
                $order_status = "Processing";
            }

            $jsonArray['data'][] = array(
                'sr_no' => $start + $key + 1,
                'order_id' => $val['order_uuid']?$val['order_uuid']:'---',
                'name' => $full_name,
                'role_type' => ($val['role_id']==2)?"Wholesaler":"Reseller",
                'mode_of_payment' => ($val['mode_of_payment'])?$val['mode_of_payment']:"",
                'payId' => ($val['payId'])?$val['payId']:"",
                'amount'=>($val['amount'])?$val['amount'].'/-':0,
                'payment_status'=>$paymentStatus,
                'order_status'=>$order_status,
                'created_at'=>date("d/m/Y", strtotime($val['created_at'])),
                //'action' => $view
				'action' => ($shipStatus)?$view.' '.$ship:$view,
            );
        }

        echo json_encode($jsonArray); exit;
        echo $this->input->post('draw'); exit;

    }
    public function ship($id){

         push_notification_test_cron('f1zNy9BwRTU:APA91bGTPQGDtp1B08X8CPzNOlwZzBHEJRsgsMpAJR73bAtmbzYHU9iC7RsrmBdfkHhw7-OZm0hxOrpK3nG-el2WEJWubCVuRHThXrWsAqMM4P-h9fywg4nX65-ALssTnR0cjrOBalrf','order ship successfull',$id);
      /*  $value = '9987199871';
        $mobileregex = "/^[6-9][0-9]{9}$/" ;
        echo "Hi ".preg_match($mobileregex, $value);die('jkj'); */
        /*if(true){
                $mail_to      = 'pawankumar.joshi@dotsquares.com';
                $subject      = 'Tesr 2';
                $mail_extra   = '';
                $mail_cc      = '';
                $mail_bcc     = '';
                if (!empty($mail_extra)) {
                    $mail_bcc = $mail_extra;
                }

                $sender_email = ADMIN_EMAIL;
                $sender_name  = ADMIN_NAME;


                $message_data = '<p>Dear Live User,</p>
                                     <p>We have received your password request, please click on below link to reset your password:</p>';



                $this->sendmail($mail_to, $subject, $message_data, $mail_cc, $mail_bcc, $sender_email, $sender_name, false);

        }
        die('Mail Send');*/

		if(!empty($id)){

			$where_key = array('order_id' => $id);
			$check_shiping_data = $this->product->getRecords($this->product->table_order_shiping, $where_key);
            if(empty($check_shiping_data)){
				$orderWhere = array("O.order_uuid"=>$id);
				$orderSelect = 'O.created_at as orderCreated, O.amount as orderSellingAmount, O.shipping_charge as shippingCharges, O.quantity as productQuantity, P.*, PM.*, U.*, D.*';
				$getOrderData = $this->product->getOrderPaymentData($orderWhere, $orderSelect);
            
				$paymentWhere = array("order_uuid"=>$id);
				$getOrderPaymentData = $this->product->getRecords($this->product->table_payment, $paymentWhere);
      			$uniqueIdForShipOrder = $getOrderPaymentData[0]['id'];

				$ch = curl_init();
				$headers = array(
					'Accept: application/json',
					'Content-Type: application/json'
				);

				$service_url = 'https://apiv2.shiprocket.in/v1/external/auth/login';

				curl_setopt($ch, CURLOPT_URL, $service_url);
				curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				curl_setopt($ch, CURLOPT_HEADER, 0);
				$body = '{
						  "email": "wasimakram@dotsquares.com",
						  "password": "Dots@123"
						}';
				curl_setopt($ch, CURLOPT_POSTFIELDS,$body);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

				// Timeout in seconds
				curl_setopt($ch, CURLOPT_TIMEOUT, 30);

				$data = curl_exec($ch);
				if(curl_errno($ch)){
					echo 'Request Error:' . curl_error($ch);
				}
				$response = json_decode($data, true);

                if(isset($response['status_code']) && $response['status_code'] == 400){
                    $this->messages->add($response['message'],'error');
                    redirect('admin/order/index');
                }
				$token = $response['token'];


				$ch1 = curl_init();
				$headers1 = array(
					'Accept: application/json',
					'Authorization:Bearer '. $token,
					'Content-Type: application/json',
				);

				$service_url1 = 'https://apiv2.shiprocket.in/v1/external/orders/create/adhoc';

				curl_setopt($ch1, CURLOPT_URL, $service_url1);
				curl_setopt($ch1, CURLOPT_HTTPHEADER, $headers1);
				curl_setopt($ch1, CURLOPT_HEADER, 0);

				foreach ($getOrderData['data'] as $orderkey => $ordervalue) {
					$items[$orderkey]["name"] = $ordervalue['product_name'];
					$items[$orderkey]["sku"] = $ordervalue['sku'];
					$items[$orderkey]["units"] = $ordervalue['productQuantity'];
					$items[$orderkey]["selling_price"] = $ordervalue['orderSellingAmount'];
					$items[$orderkey]["discount"] = "";
					$items[$orderkey]["tax"] = "";
					$items[$orderkey]["hsn"] = "62114210";
				}
                
				$newArray = array();
				$newArray["order_id"] = $uniqueIdForShipOrder;  //payment unique id
				$newArray["order_date"] = $getOrderData['data'][0]["orderCreated"];
				$newArray["pickup_location"] = "Primary";
				$newArray["channel_id"] = "54317";
				$newArray["comment"] = "Reseller: Vedika Enterprises";
				$newArray["billing_customer_name"] = $getOrderData['data'][0]["name"];
				$newArray["billing_last_name"] = "";
				$newArray["billing_address"] = $getOrderData['data'][0]["address_line1"];
				$newArray["billing_address_2"] = $getOrderData['data'][0]["address_line2"].' '.$getOrderData['data'][0]["landmark"];
				$newArray["billing_city"] = $getOrderData['data'][0]["city"];
				$newArray["billing_pincode"] = $getOrderData['data'][0]["pincode"];
				$newArray["billing_state"] = $getOrderData['data'][0]["state"];
				$newArray["billing_country"] = "India";
				$newArray["billing_email"] = $getOrderData['data'][0]["email"];
				$newArray["billing_phone"] = $getOrderData['data'][0]["mobile_number"];
				$newArray["shipping_is_billing"] = true;
				$newArray["shipping_customer_name"] = "";
				$newArray["shipping_last_name"] = "";
				$newArray["shipping_address"] = "";
				$newArray["shipping_address_2"] = "";
				$newArray["shipping_city"] = "";
				$newArray["shipping_pincode"] = "";
				$newArray["shipping_country"] = "";
				$newArray["shipping_state"] = "";
				$newArray["shipping_email"] = "";
				$newArray["shipping_phone"] = "";
				$newArray["order_items"] = $items;
				
                $newArray["payment_method"] = "prepaid";
				$newArray["shipping_charges"] = $getOrderData['data'][0]["shippingCharges"];
				$newArray["giftwrap_charges"] = 0;
				$newArray["transaction_charges"] = 0;
				$newArray["total_discount"] = 0;
				
                $newArray["weight"] = !empty($getOrderData['data'][0]["weight"])?$getOrderData['data'][0]["weight"] / 1000 : 0; //converted in grams
				$newArray["sub_total"] = $getOrderData['data'][0]["amount"];
				//static
				$newArray["length"] = 10;
				$newArray["breadth"] = 10;
				$newArray["height"] = 10;


				$body1 = json_encode($newArray);

				curl_setopt($ch1, CURLOPT_POSTFIELDS,$body1);
				curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);

				// Timeout in seconds
				curl_setopt($ch1, CURLOPT_TIMEOUT, 30);

				$data = curl_exec($ch1);
				if(curl_errno($ch1)){
					echo 'Request Error:' . curl_error($ch1);
				}
				$response = json_decode($data, true);
                //prd($response); die;

                if(!empty($response)){
                    if($response['status_code'] == 422){
                        foreach ($response['errors'] as $key => $value) {
                             foreach ($value as $key => $val) {
                               $showErrors[] = $val;
                            }
                        }
                    }

                }

                if(isset($response['status_code']) && $response['status_code'] == 403){
                    $this->messages->add($response['message'],'error');
                    redirect('admin/order/index');
                }

                if(isset($response['status_code']) && $response['status_code'] == 422){
                   $missingValue='';
                    if(!empty($showErrors)){
                        foreach ($showErrors as $key => $err) {
                            if ($key == 0) {
                                $missingValue= $err;
                            }else{
                                $missingValue .= '</br>'.$err;
                            }

                        }

                    }
                   
                    $this->messages->add($missingValue,'error');
                    redirect('admin/order/index');
                }



				$shipment_order_id = $response['order_id'];
				$shipment_id = $response['shipment_id'];
				$shipment_status = $response['status'];
				$shipment_status_code = $response['status_code'];


                $where_array = array(
                    'order_uuid' => $id
                );
                $order_update_array = array(
                    'shipment_order_id' => $shipment_order_id,
                    'shipment_id' => $shipment_id,
                    'order_status' => 3

                );

                $this->product->save($this->product->table_order,$order_update_array,$where_array);


				$this->messages->add('Item shipped','success');

                // push_notification_test_cron();
				redirect('admin/order/index');
			}else{
				$this->messages->add('Item has been already shipped','error');
				redirect('admin/order/index');
			}
		}else{
			$this->messages->add('Order id is missing','error');
            redirect('admin/order/index');
		}

    }
    /*
    Track Shipping Order
    */

    public function track_order($order_id){
        if(!empty($order_id)){
            $where_key = array('order_id' => $order_id);
            $check_shiping_data = $this->product->getRecords($this->product->table_order_shiping, $where_key);
            //Array ( [0] => Array ( [id] => 35 [shipment_order_id] => 8869761 [shipment_id] => 8791743 [shipment_status] => NEW [shipment_status_code] => 1 [order_id] => b556412f-3b22-11e9-a954-025e99f7e65a ) )

            if(!empty($check_shiping_data)){
                $shipment_id = $check_shiping_data[0]['shipment_id'];
                if(!empty($shipment_id)){
                    $ch = curl_init();
                    $headers = array(
                        'Accept: application/json',
                        'Content-Type: application/json'
                    );

                    $service_url = 'https://apiv2.shiprocket.in/v1/external/auth/login';

                    curl_setopt($ch, CURLOPT_URL, $service_url);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                    curl_setopt($ch, CURLOPT_HEADER, 0);
                    $body = '{
                                "email": "wasimakram@dotsquares.com",
                                "password": "Dots@123"
                            }';
                    curl_setopt($ch, CURLOPT_POSTFIELDS,$body);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                    // Timeout in seconds
                    curl_setopt($ch, CURLOPT_TIMEOUT, 30);

                    $data = curl_exec($ch);
                    if(curl_errno($ch)){
                        echo 'Request Error:' . curl_error($ch);
                    }
                    $response = json_decode($data, true);

                    if(isset($response['status_code']) && $response['status_code'] == 400){
                        $this->messages->add($response['message'],'error');
                        redirect('admin/order/index');
                    }
                    $token = $response['token'];


                    $ch1 = curl_init();
                    $headers1 = array(
                        'Accept: application/json',
                        'Authorization:Bearer '. $token,
                        'Content-Type: application/json',
                    );

                    $service_url1 = 'https://apiv2.shiprocket.in/v1/external/courier/track/shipment/'.$shipment_id;

                    curl_setopt($ch1, CURLOPT_URL, $service_url1);
                    curl_setopt($ch1, CURLOPT_HTTPHEADER, $headers1);
                    curl_setopt($ch1, CURLOPT_HEADER, 0);

                    $newArray = [];
                    $body1 = json_encode($newArray);

                    //curl_setopt($ch1, CURLOPT_POSTFIELDS,$body1);
                    curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);

                    // Timeout in seconds
                    curl_setopt($ch1, CURLOPT_TIMEOUT, 30);

                    $data = curl_exec($ch1);
                    if(curl_errno($ch1)){
                        echo 'Request Error:' . curl_error($ch1);
                    }
                    $response = json_decode($data, true);

                    $index['page_title'] = ':: Track Order ::';
                    $content['form_title'] = 'Track Order';
                    $layout = 'admin-layout';
                    $view_file =  'admin/orders/track';
                    $content['track_order'] = $response;
                
                }else{
                    $this->messages->add('This Order is not shipped yet','error');
                    redirect('admin/order/index');
                }
            }else{
                $this->messages->add('This Order is not shipped yet','error');
                redirect('admin/order/index');
            }
        }else{
            $this->messages->add('Order not found','error');
            redirect('admin/order/index');
        }
        $this->templates->set($layout);
        $content['layout'] = $view_file;
        $this->templates->set_data('index',$index);
        $this->templates->set_data('content',$content);
        $this->templates->load();
    }

    public function get_orders_old(){

        $order_by = array();
        $length = $this->input->post('length');
        $start = $this->input->post('start');
        if(empty($length)){
            $length = 10;
            $start = 0;
        }
        $columnData = array(
            'sr_no',
            'O.id',
            'U.id',
            'U.role_id',
            'P.id',
            'O.purchase_type',
            'O.quantity',
            'O.amount',
            'O.order_status',
            'O.created_at',
            'action'
        );
        $sortData = $this->input->post('order');
        $order_by[0] = $columnData[$sortData[0]['column']];
        $order_by[1] = $sortData[0]['dir'];
        $searchData = $this->input->post('searchBox');
        $userdata = $this->input->post('userdata');
        $usertype = $this->input->post('usertype');
        $where = '';
        $and=' and ';
         if($searchData){
            $searchData = trim($searchData);

            $where.= '(P.product_name like "%'.$searchData.'%" OR O.created_at like "%'.$searchData.'%" OR O.order_status like "%'.$searchData.'%" OR O.order_uuid like "%'.$searchData.'%" OR O.quantity LIKE "'.$searchData.'" OR CONCAT(U.first_name," ",U.last_name) LIKE "'.$searchData.'")';

        }

        if($usertype){
            if($searchData){
                $where.= $and.'U.role_id =  '.$usertype;
            }else{
                $where.='U.role_id =  '.$usertype;
            }

        }

        if($userdata){
            if($searchData || $usertype){
                $where.= $and.'U.id =  '.$userdata;
            }else{
                $where.='U.id =  '.$userdata;
            }

        }

        $List_Array = $this->product->getOrderData($where,$select =  'U.role_id,P.product_name,U.first_name,U.last_name,O.*',$order_by, $start, $length,$where_in = false,$where_not_in = false,'order_uuid');

        $orderList = $List_Array['data'];

        $totalData = $List_Array['total'];
        $jsonArray=array(
            'draw'=>$this->input->post('draw'),
            'recordsTotal'=>$totalData,
            'recordsFiltered'=>$totalData,
            'data'=>array(),
        );
    
        foreach($orderList as $key => $val){

            $view = '<a href="'.base_url('admin/order/view/'.$val['id']).'" class="tableIcon"><i class="fa fa-eye" title="View"></i></a>';

            $full_name = ($val['first_name'])?ucfirst($val['first_name']." ".$val['last_name']):'---';
            if($val['order_status']==1){
            $status = "pending";
            }else if($val['order_status']==2){
                $status = "Paid";
             }else if($val['order_status']==0){
                 $status = "failed";
             }else{
                $status = "pending";
             }

            $jsonArray['data'][] = array(
                'sr_no' => $start + $key + 1,
                'order_id' => $val['order_uuid']?$val['order_uuid']:'---',
                'name' => $full_name,
                'role_type' => ($val['role_id']==2)?"Vender":"Reseller",
                'pname' => $val['product_name']?$val['product_name']:'---',
                'purchase_type' => ($val['purchase_type']==1)?'Single':'Set',
                'quantity' => $val['quantity']?$val['quantity']:'---',
                'amount'=>($val['amount'])?$val['amount'].'/-':0,
                'order_status'=>$status,
                'created_at'=>date("d/m/Y", strtotime($val['created_at'])),
                'action' => $view
            );
        }

        echo json_encode($jsonArray); exit;
        echo $this->input->post('draw'); exit;

    }


public function status(){

    $sID = $this->input->post('sID');
    $jsonArray=array('flag'=>false);
    $where_array = array(
        'id' => $sID
    );
    $page_update_array = array(
        'is_active' => $this->input->post('sStatus')
    );
    if($this->product->save($this->product->table_users,$page_update_array, $where_array)){
        $jsonArray['flag'] = true;
    }
    echo json_encode($jsonArray); exit;
}

public function delete(){

    $sID = $this->input->post('sID');
    $jsonArray = array('flag' => false);
    $where_array = array(
     'id' => $sID
 );
    $page_update_array = array(
        'is_deleted' => '1'
    );
    if($this->product->save($this->product->table_users,$page_update_array, $where_array)){
            #echo $this->db->last_query();die;
        $jsonArray['flag'] = true;
    }
    echo json_encode($jsonArray); exit;
}
/**
     * this is a generic function to get records of parking operative
     * function having no parameter
     * function using template admin-login
     * @access public
    */
    public function get_user_list(){
        $where_key = array(
            'role_id' => $this->input->post('usertype'),
            'is_active' => '1',
            'is_deleted' => '0'
        );
        //getting data of parking operative to display in customer section for filtration
        $order_by[0] = 'first_name';
        $order_by[1] = 'ASC';
        $user_details = $this->users->getRecords($this->users->table_users,$where_key,'id,first_name,last_name,email',null,null,$order_by);
        echo json_encode($user_details); exit;
    }
    public function get_all_user_list(){
        $where_key = array(
            'is_active' => '1',
            'is_deleted' => '0'
        );
        //getting data of parking operative to display in customer section for filtration
        $order_by[0] = 'first_name';
        $order_by[1] = 'ASC';
        $user_details = $this->users->getRecords($this->users->table_users,$where_key,'id,first_name,last_name',null,null,$order_by);
        echo json_encode($user_details); exit;
    }
}
